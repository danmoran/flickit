//
//  PlaySounds.m
//  iPower
//
//  Created by Daniel Moran on 5/2/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "PlaySounds.h"
#import "Constants.h"

@interface PlaySounds ()

@property (nonatomic, retain) NSError *error;
@property (nonatomic, retain) AVAudioPlayer *backgroundAudio;
@property (nonatomic, retain) AVAudioPlayer *draggingAudio;
@property (nonatomic, retain) AVAudioPlayer *snortAudio;
//@property (nonatomic, retain) AVAudioPlayer *snortStartingAudio;
@property (nonatomic, retain) AVAudioPlayer *snortEndingAudio;
@property (nonatomic, retain) AVAudioPlayer *explosionAudio;


@end


@implementation PlaySounds

@synthesize error;
@synthesize backgroundAudio, draggingAudio, snortAudio, snortEndingAudio, explosionAudio;
@synthesize isSnortingAudioPlaying;

+ (id)playSounds { 
    
    return [[[self alloc] init] autorelease]; 
}

- (id)init {
	if ((self = [super init])) {
        
	}
	return self;
}
-(void) initializeSounds {
    
    [self playDraggingSound];
    [self turnDraggingSoundDown];
    [self playSnortSound];
    [self snortSoundVolumeDown];
    [self playBackgroundSound];
    
//    if (!self.backgroundAudio.isPlaying) {
//        [self playBackgroundSound];
//    }
    
}

-(void) playBackgroundSound {
    
    NSUserDefaults *playUserSetting = [NSUserDefaults standardUserDefaults];
    BOOL shouldPlay = [playUserSetting boolForKey:kPlayBackgroundMusic];
    BOOL isBackgroundMusicCurrentlyPlaying = self.backgroundAudio.isPlaying;
    
//    NSURL *urlBackground = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Fireworks.mp3", [[NSBundle mainBundle] resourcePath]]];
//    self.backgroundAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlBackground error:&error]autorelease];
//    self.backgroundAudio.numberOfLoops = -1;
    
    if (shouldPlay) {
        if (!isBackgroundMusicCurrentlyPlaying) {
            
            NSURL *urlBackground = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Fireworks.mp3", [[NSBundle mainBundle] resourcePath]]];
            self.backgroundAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlBackground error:&error]autorelease];
            self.backgroundAudio.numberOfLoops = -1;
            [self.backgroundAudio play];
        } else {
            
        }
        
    } else {
        [self.backgroundAudio stop];
    }
}

//-(void) stopBackgroundSound {
//    
//    [self.backgroundAudio pause];
//
//}

-(void) playDraggingSound {
    
    NSUserDefaults *playUserSetting = [NSUserDefaults standardUserDefaults];
    BOOL shouldPlay = [playUserSetting boolForKey:kPlayDraggingSound];
    
    NSURL *urlDrag = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/ScrapeLoop.mp3", [[NSBundle mainBundle] resourcePath]]];
    self.draggingAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlDrag error:&error]autorelease];
    self.draggingAudio.numberOfLoops = -1;
    
    if (shouldPlay) {
        [self.draggingAudio play];
        
    } else {
        
        [self.draggingAudio stop];   
    }
    
}

-(void) turnDraggingSoundUp {
    
    self.draggingAudio.volume = 0.5f;
}

-(void) turnDraggingSoundDown {
    
    self.draggingAudio.volume = 0.0f;
}

-(void) playSnortSound {
    
    NSUserDefaults *playUserSetting = [NSUserDefaults standardUserDefaults];
    BOOL shouldPlay = [playUserSetting boolForKey:kPlayConsumeSound];
    
    NSURL *urlSnort = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Snort.mp3", [[NSBundle mainBundle] resourcePath]]];
    self.snortAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlSnort error:&error]autorelease];
    self.snortAudio.numberOfLoops = -1;
    
    if (shouldPlay) {
        [self.snortAudio play];
        isSnortingAudioPlaying = YES;
    } 
}

-(void) snortSoundVolumeDown {
    
    self.snortAudio.volume = 0.0f;
}

-(void) snortSoundVolumeUp {
    
    self.snortAudio.volume = 0.5f;
}


//-(void) playSnortEndingSound {
//    
//    NSURL *urlSnort = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/snortRelease.mp3", [[NSBundle mainBundle] resourcePath]]];
//    self.snortEndingAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlSnort error:&error]autorelease];
//    self.snortEndingAudio.numberOfLoops = 0;
//    
//    [self.snortEndingAudio play];
//}
-(void) playExplosionSound {
    
    NSURL *urlBackground = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/explosion.mp3", [[NSBundle mainBundle] resourcePath]]];
    self.explosionAudio = [[[AVAudioPlayer alloc] initWithContentsOfURL:urlBackground error:&error]autorelease];
    self.explosionAudio.numberOfLoops = 0;
    
    [self.explosionAudio play];
}

//-(void) stopExplosionSound {
//    
//    [self.explosionAudio stop];
//}



- (void)dealloc {

//    [error release];
//    [backgroundAudio release];
//    [draggingAudio release];
//    [snortAudio release]; 

    [super dealloc];
}

@end
