//
//  TiltingObjects.h
//  iPower
//
//  Created by Daniel Moran on 5/7/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

//#import <Foundation/Foundation.h>
//
//@interface TiltingObjects : NSObject
//
//
//+(NSArray *) calculateSubstanceDistanceAndAssignInertiaLevel:(NSArray *)substances;
//+(NSMutableArray *) moveObjectBasedOnTiltAngleForSubstances:(NSMutableArray *)substances havingAngle:(CGFloat) angle withAcceleration:(UIAcceleration *) acceleration; 
//
//@end
