//
//  MainSceneViewController.h
//  iPower
//
//  Created by Daniel Moran on 3/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MixturesObject;
@class SubstanceObject;


@interface MainSceneViewController : UIViewController <UITextFieldDelegate, UIAccelerometerDelegate, UIPopoverControllerDelegate>

@property (nonatomic, retain) SubstanceObject *substanceObject;
@property (nonatomic, retain) NSMutableArray *views;
@property (nonatomic, retain) MixturesObject *mixtureObject;
@property (retain, nonatomic) IBOutlet UIButton *resetBtn;
@property (retain, nonatomic) IBOutlet UIButton *pausePlayBtn;
@property (retain, nonatomic) IBOutlet UIButton *menuBtn;
@property (retain, nonatomic) IBOutlet UIView *gamePaused;
@property (retain, nonatomic) IBOutlet UIButton *closeGamePausedBtn;
@property (retain, nonatomic) IBOutlet UIButton *exportBtn;
@property (retain, nonatomic) IBOutlet UIButton *consumeBtn;

@property (nonatomic, assign) CGFloat w;
@property (nonatomic, assign) CGFloat h;
@property (nonatomic, assign) CGFloat viewCenterX;
@property (nonatomic, assign) CGFloat viewCenterY;
//@property (nonatomic, assign) BOOL shouldPlayBackgroundMusic;
@property (retain, nonatomic) IBOutlet UIView *shareDialogView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *actionBtnItem;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *menuBtnItem;
@property (retain, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (retain, nonatomic) IBOutlet UIView *popupMessageView;
@property (retain, nonatomic) IBOutlet UILabel *popupMessageLbl;


-(void) loadLastSubstanceObjects;
-(void) loadSelectedMixtureObject:(MixturesObject *)mixture;
//-(void) loadSubstanceObjects;
-(void) loadSubstanceObjectsWithMaxDist:(int) dist1 minDist:(int)dist2 startingCountAt:(int) startingCount randomlocation:(BOOL) useRandomPlacement;
-(void) presentMenuViewController; 
-(void) presentSavedMixturesViewController;
- (IBAction)presentLabView:(id)sender; 
-(void) saveMixtureInDBWithName:(NSString *)name;
-(void) updateSubstanceTypesOnScreenWithValuesIn:(NSDictionary *) containerValues;
-(UIImage *) imageByRenderingMainSceneView;
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController;
-(void) setBackgroundImageWithImage:(UIImage *)image;
-(void) setViewOrientationParametersForPopovers;

@end
