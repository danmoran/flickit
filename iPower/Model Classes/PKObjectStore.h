//
//  PKObjectStore.h
//  PKTest
//
//  Created by Petr Homola on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKPersistentObject.h"
#import "PKObjectUID.h"
#import "PKPredicate.h"
#import "PKObjectStoreImpl.h"

@interface PKObjectStore : NSObject {
	NSOperationQueue* operationQueue;
	//NSMutableDictionary* UIDs;
	//NSMutableDictionary* objectsByUID;			// for the default in-memory implementation
	//NSMutableDictionary* objectsByClassName;	// for the default in-memory implementation
	PKObjectStoreImpl* underlyingStore;
}

+ (PKObjectStore*)defaultObjectStore;
+ (void)closeDefaultObjectStore;
//+ (PKObjectStore*)objectStoreWithFilePath:(NSString*)_path;
- (void)fetchObjectsOfClass:(Class)cls callback:(void(^)(NSArray*))callback;
- (void)fetchObjectsOfClass:(Class)cls predicate:(PKPredicate*)predicate callback:(void(^)(NSArray*))callback;
- (void)fetchObjectsOfClass:(Class)cls objectCallback:(void(^)(id<PKPersistentObject>))callback;
- (void)fetchObjectsOfClass:(Class)cls asyncObjectCallback:(void(^)(id<PKPersistentObject>))callback;
- (void)storeObject:(id<NSObject,PKPersistentObject>)object callback:(void(^)(BOOL))callback;
- (void)deleteObject:(id<NSObject,PKPersistentObject>)object callback:(void(^)(BOOL))callback;
- (id<PKPersistentObject>)emptyObjectOfClass:(Class)cls;
//- (PKObjectUID*)UIDOfObject:(id<PKPersistentObject>)object;
- (void)fulltextSearchWithClass:(Class)cls attributeName:(NSString*)attName pattern:(NSString*)pattern callback:(void(^)(NSArray*))callback;

@end
