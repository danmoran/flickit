//
//  Constants.h
//  iPower
//
//  Created by Daniel Moran on 4/13/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#define kIsAppFirstTimeOpened @"isFirstTimeOpened"


/// commonly used images
#define kConsumeBtn @"consume.png"
#define kConsumeBtnOn @"consumeON.png"
#define kConsumePadBtn @"consumePad.png"
#define kConsumePadBtnOn @"consumeONPad.png"
/// substance images names
#define kSubstanceWhite1 @"substanceWhite1.png"
#define kSubstanceWhite2 @"substanceWhite2.png"
#define kSubstanceWhite3 @"substanceWhite3.png"
#define kSubstanceWhite4 @"substanceWhite4.png"
#define kSubstanceWhite5 @"substanceWhite5.png"
#define kSubstanceWhite6 @"substanceWhite6.png"
#define kSubstanceWhite7 @"substanceWhite7.png"
#define kSubstanceWhite8 @"substanceWhite8.png"
#define kSubstanceWhite9 @"substanceWhite9.png"
#define kSubstanceWhite10 @"substanceWhite10.png"

#define kSubstanceGreen1 @"substanceGreen1.png"
#define kSubstanceGreen2 @"substanceGreen2.png"
#define kSubstanceGreen3 @"substanceGreen3.png"
#define kSubstanceGreen4 @"substanceGreen4.png"
#define kSubstanceGreen5 @"substanceGreen5.png"
#define kSubstanceGreen6 @"substanceGreen6.png"
#define kSubstanceGreen7 @"substanceGreen7.png"
#define kSubstanceGreen8 @"substanceGreen8.png"
#define kSubstanceGreen9 @"substanceGreen9.png"
#define kSubstanceGreen10 @"substanceGreen10.png"

#define kSubstanceBlue1 @"substanceBlue1.png"
#define kSubstanceBlue2 @"substanceBlue2.png"
#define kSubstanceBlue3 @"substanceBlue3.png"
#define kSubstanceBlue4 @"substanceBlue4.png"
#define kSubstanceBlue5 @"substanceBlue5.png"
#define kSubstanceBlue6 @"substanceBlue6.png"
#define kSubstanceBlue7 @"substanceBlue7.png"
#define kSubstanceBlue8 @"substanceBlue8.png"
#define kSubstanceBlue9 @"substanceBlue9.png"
#define kSubstanceBlue10 @"substanceBlue10.png"

#define kSubstancePink1 @"substancePink1.png"
#define kSubstancePink2 @"substancePink2.png"
#define kSubstancePink3 @"substancePink3.png"
#define kSubstancePink4 @"substancePink4.png"
#define kSubstancePink5 @"substancePink5.png"
#define kSubstancePink6 @"substancePink6.png"
#define kSubstancePink7 @"substancePink7.png"
#define kSubstancePink8 @"substancePink8.png"
#define kSubstancePink9 @"substancePink9.png"
#define kSubstancePink10 @"substancePink10.png"

#define kSubstanceRed1 @"substanceRed1.png"
#define kSubstanceRed2 @"substanceRed2.png"
#define kSubstanceRed3 @"substanceRed3.png"
#define kSubstanceRed4 @"substanceRed4.png"
#define kSubstanceRed5 @"substanceRed5.png"
#define kSubstanceRed6 @"substanceRed6.png"
#define kSubstanceRed7 @"substanceRed7.png"
#define kSubstanceRed8 @"substanceRed8.png"
#define kSubstanceRed9 @"substanceRed9.png"
#define kSubstanceRed10 @"substanceRed10.png"


/// background image and descriptions.
#define kBackgroundKeyName @"backgroundName"

#define kBackground1 @"backgroundMarble.png"
#define kBackground2 @"backgroundWoodBrown.png"
#define kBackground3 @"backgroundLeather.png"
#define kBackground4 @"backgroundMetal.png"

#define kBackgroundPad1 @"backgroundMarble_iPad.png"
#define kBackgroundPad2 @"backgroundWoodBrown_iPad.png"
#define kBackgroundPad3 @"backgroundLeather_iPad.png"
#define kBackgroundPad4 @"backgroundMetal_iPad.png"

#define kBackgroundSelection1 @"marbleSelection.png"
#define kBackgroundSelection2 @"woodBrownSelection.png"
#define kBackgroundSelection3 @"leatherSelection.png"
#define kBackgroundSelection4 @"metalSelection.png"

//#define kBackgroundSelection1 @"marbleSelection.png"
//#define kBackgroundSelection2 @"woodSelection.png"
//#define kBackgroundSelection3 @"leatherSelection.png"
//#define kBackgroundSelection4 @"metalSelection.png"
#define kBackgroundSelectionSelected1 @"marbleSelectionSelected.png"
#define kBackgroundSelectionSelected2 @"woodSelectionSelected.png"
#define kBackgroundSelectionSelected3 @"leatherSelectionSelected.png"
#define kBackgroundSelectionSelected4 @"metalSelectionSelected.png"

#define kBackgroundDescription1 @"surface 1 description"
#define kBackgroundDescription2 @"surface 2 description"
#define kBackgroundDescription3 @"surface 3 description"
#define kBackgroundDescription4 @"surface 4 description" 

/// container names
#define kContainer1 @"container1"
#define kContainer2 @"container2"
#define kContainer3 @"container3"
#define kContainer4 @"container4"
#define kContainerx @"containerx"

/// touch contants including object size and CGRectIntersectsRect size
#define kNumberOfSubtanceObjects 150
#define kPadMultiplier 2
#define kObjectSize 10
#define kTouchSize 30
#define kTOuchSizeBuffer 60
#define kCircleHolderPhone 41
#define kCircleHolderPad 72

#define kMinInertiaValue 10.0f
#define kMaxInertiaValue 3.0f

/// when apps is opened for the first time
#define kWasPreviouslyOpened @"wasPreviouslyOpened"
#define kPlayBackgroundMusic @"shouldPlayBackgroundMusic"
#define kPlayDraggingSound @"playDraggingSound"
#define kPlayConsumeSound @"playConsumeSound"

#define kContainerDescription1 @"Container1 description"
#define kContainerDescription2 @"Container2 description"
#define kContainerDescription3 @"Container3 description"
#define kContainerDescription4 @"Container4 description"