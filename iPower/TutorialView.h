//
//  TutorialView.h
//  iPower
//
//  Created by Daniel Moran on 7/27/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialView : UIView


@property (nonatomic, assign) UIDeviceOrientation deviceOrientation;

//- (id)initWithFrame:(CGRect)frame andHavingOrientation:(UIDeviceOrientation) orientation;
+ (id)tutorial;
-(UIImageView *) tutorialViewhavingFrame:(CGRect )frame;
-(void) removeTutotialView;
@end
