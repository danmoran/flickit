//
//  NSObjectPKAdditions.m
//  PKTest
//
//  Created by Petr Homola on 6/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSObjectPKAdditions.h"

@implementation NSObject (PKAdditions)

- (NSString*)className {
	return NSStringFromClass([self class]);
}

@end
