//
//  PlaySounds.h
//  iPower
//
//  Created by Daniel Moran on 5/2/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface PlaySounds : NSObject <AVAudioPlayerDelegate>

//@property (nonatomic, assign) BOOL isDragginAudioPlaying;
@property (nonatomic, assign) BOOL isSnortingAudioPlaying;


+ (id)playSounds;
-(void) initializeSounds;
-(void) playBackgroundSound;
//-(void) stopBackgroundSound;
-(void) playDraggingSound;
-(void) turnDraggingSoundUp;
-(void) turnDraggingSoundDown;
-(void) playSnortSound;
-(void) snortSoundVolumeDown;
-(void) snortSoundVolumeUp;
//-(void) playSnortEndingSound;
-(void) playExplosionSound;



@end
