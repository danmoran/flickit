//
//  PKObjectStoreImpl.h
//  DBLib
//
//  Created by Petr Homola on 6/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKObjectStoreImpl : NSObject {
	id underlyingStore;
}

- (id)initWithDocumentFilePath:(NSString*)path;
- (BOOL)storeObject:(id)object;
- (BOOL)removeObject:(id)object;
- (NSArray*)queryWithClass:(Class)cls;
- (NSArray*)queryWithClass:(Class)cls attributeName:(NSString*)att value:(NSString*)value;

@end
