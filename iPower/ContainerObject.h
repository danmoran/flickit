//
//  ContainerObject.h
//  iPower
//
//  Created by Daniel Moran on 4/13/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MixturesObject;
@class SubstanceObject;

@interface ContainerObject : NSObject

@property (nonatomic, retain) NSMutableDictionary *containers;

+ (id)container;
-(NSDictionary *) updateContainerlevelsUsingMixtureObject:(NSMutableArray *)mixture;
-(NSMutableArray *) udpateMixtureSubstanceTypesUsing:(NSDictionary *) containerLevels forSubstances:(NSMutableArray *) substances;
-(void) setImageForSubstanceObject:(SubstanceObject *)substance havingImageName :(NSMutableString *) imageName;

@end
