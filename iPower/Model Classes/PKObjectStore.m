//
//  PKObjectStore.m
//  PKTest
//
//  Created by Petr Homola on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PKObjectStore.h"

static PKObjectStore* defaultStore = nil;

@implementation PKObjectStore

- (id)init {
	if ((self = [super init])) {
		//objectsByUID = [[NSMutableDictionary alloc] init];
		//objectsByClassName = [[NSMutableDictionary alloc] init];
		//UIDs = [[NSMutableDictionary alloc] init];
        operationQueue = [[NSOperationQueue alloc] init];
	}
	return self;
}

+ (PKObjectStore*)defaultObjectStore {
	if (defaultStore == nil) {
		defaultStore = [[PKObjectStore alloc] init];
		defaultStore->underlyingStore = [[PKObjectStoreImpl alloc] initWithDocumentFilePath: @"default.db"];
	}
	return defaultStore;
}

+ (void)closeDefaultObjectStore {
    defaultStore = nil;
}

/*+ (PKObjectStore*)objectStoreWithFilePath:(NSString*)_path {
	return nil;
}*/

- (void)fetchObjectsOfClass:(Class)cls callback:(void(^)(NSArray*))callback {
	//callback([[objectsByClassName objectForKey: NSStringFromClass(cls)] allObjects] ?: [NSArray array]);
	//callback([underlyingStore queryWithClass: cls]); // synchronous
    [operationQueue addOperation: [NSBlockOperation blockOperationWithBlock: ^{
        NSArray* objects = [underlyingStore queryWithClass: cls];
        [[NSOperationQueue mainQueue] addOperation: [NSBlockOperation blockOperationWithBlock: ^{
            callback(objects);
        }]];
    }]];
}

- (void)fetchObjectsOfClass:(Class)cls predicate:(PKPredicate*)predicate callback:(void(^)(NSArray*))callback {
	[self fetchObjectsOfClass: cls callback: ^(NSArray* objects) {
		NSMutableArray* filteredObjects = [NSMutableArray arrayWithCapacity: [objects count]];
		for (id<PKPersistentObject> object in objects) {
			if ([predicate matchesObject: object]) [filteredObjects addObject: object];
		}
        callback(filteredObjects);
	}];
}

- (void)fetchObjectsOfClass:(Class)cls objectCallback:(void(^)(id<PKPersistentObject>))callback {
	[self fetchObjectsOfClass: cls callback: ^(NSArray* objects) {
		for (id<PKPersistentObject> object in objects) {
			callback(object);
		}
	}];
}

- (void)fetchObjectsOfClass:(Class)cls asyncObjectCallback:(void(^)(id<PKPersistentObject>))callback {
	if (operationQueue == nil) operationQueue = [[NSOperationQueue alloc] init];
	[self fetchObjectsOfClass: cls callback: ^(NSArray* objects) {
		for (id<PKPersistentObject> object in objects) {
			[operationQueue addOperation: [NSBlockOperation blockOperationWithBlock: ^(void) {
				callback(object);
			}]];
		}
	}];
}

- (void)storeObject:(id<NSObject,PKPersistentObject>)object callback:(void(^)(BOOL))callback {
	/*NSValue* key = [NSValue valueWithPointer: object];
	PKObjectUID* UID = [UIDs objectForKey: key];
	if (UID == nil) {
		[UIDs setObject: UID = [PKObjectUID objectUID] forKey: key];
		[objectsByUID setObject: object forKey: UID];
		NSMutableSet* set = [objectsByClassName objectForKey: NSStringFromClass([object class])];
		if (set == nil) [objectsByClassName setObject: set = [NSMutableSet set]
											   forKey: NSStringFromClass([object class])];
		[set addObject: object];
	}
	callback(YES);*/
	callback([underlyingStore storeObject: object]);
}

- (void)deleteObject:(id<NSObject,PKPersistentObject>)object callback:(void(^)(BOOL))callback {
	/*PKObjectUID* UID = [self UIDOfObject: object];
	if (UID == nil) callback(NO);
	else {
		[objectsByUID removeObjectForKey: UID];
		NSMutableSet* set = [objectsByClassName objectForKey: NSStringFromClass([object class])];
		[set removeObject: object];
		callback(YES);
	}*/
	[underlyingStore removeObject: object];
}

- (void)fulltextSearchWithClass:(Class)cls attributeName:(NSString*)attName pattern:(NSString*)pattern callback:(void(^)(NSArray*))callback {
	[self fetchObjectsOfClass: cls callback: ^(NSArray* objects) {
		NSMutableArray* matchingObjects = [[NSMutableArray alloc] init];
		@try {
		for (id object in objects) {
			NSString* value = [object valueForKey: attName];
			if ([value rangeOfString: pattern].location != NSNotFound) [matchingObjects addObject: object];
		}
		callback(matchingObjects);
		} @finally {
			//[matchingObjects release]; // ARC
		}
	}];
}

- (id<PKPersistentObject>)emptyObjectOfClass:(Class)cls {
	return [[[cls alloc] init]autorelease]; // ARC
}

/*- (PKObjectUID*)UIDOfObject:(id<PKPersistentObject>)object {
	return [UIDs objectForKey: [NSValue valueWithPointer: object]];
}*/


@end
