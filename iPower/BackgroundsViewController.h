//
//  BackgroundsViewController.h
//  iPower
//
//  Created by Daniel Moran on 2/21/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackgroundsViewController : UIViewController <UIGestureRecognizerDelegate> {
    
    
    NSArray *surfaceImages;
    NSArray *surfaceDescription;
    NSInteger currentIndex;
    
    
    IBOutlet UIButton *selectBtn;
    IBOutlet UIButton *previousBtn;
    IBOutlet UIButton *nextBtn;
    IBOutlet UILabel *descriptionLbl;
}
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageCenter;
//@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageLeft;
//@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageRight;


-(void) setBackgroundImageHaving:(NSString *) imageName;
-(void) updateBackgroundImages;

@end
