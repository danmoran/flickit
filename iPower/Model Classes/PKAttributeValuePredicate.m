//
//  PKAttributeValuePredicate.m
//  PKTest
//
//  Created by Petr Homola on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PKAttributeValuePredicate.h"

@implementation PKAttributeValuePredicate

- (id)initWithAttributeName:(NSString*)attName value:(id)_value {
	if ((self = [super init])) {
		attributeName = [attName copy];
		value = _value; // ARC
	}
	return self;
}

- (BOOL)matchesObject:(id<PKPersistentObject>)object {
	return [[object valueForKey: attributeName] isEqual: value];
}


@end
