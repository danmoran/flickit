//
//  SaveMixtureViewController.m
//  iPower
//
//  Created by Daniel Moran on 7/10/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "SaveMixtureViewController.h"
//#import "MainSceneViewController.h"
#import "LabPageViewControlleriPad.h"
#import "MixturesObject.h"
#import "SubstanceObject.h"
#import "PKObjectStore.h"


@interface SaveMixtureViewController ()

//@property (nonatomic, retain) MainSceneViewController *mainScene;
@property (nonatomic, retain) LabPageViewControlleriPad *labPage;

@end

@implementation SaveMixtureViewController

//@synthesize mainScene = _mainScene;
@synthesize labPage = _labPage;
@synthesize saveTextField;
@synthesize selectionHandler;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

+ (id) save { 
    
    return [[[self alloc] init] autorelease]; 
}
//-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
//    
//    self.mainScene = _mainSceneViewController;
//}

-(void) setLabPageViewControlleriPad:(LabPageViewControlleriPad *) _labPageViewControlleriPad {
    
    self.labPage = _labPageViewControlleriPad;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.saveTextField becomeFirstResponder];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)cancelSaveAndDismissViewController:(id)sender {

    selectionHandler ();
}

- (IBAction)saveMixture:(id)sender {
    
    [self.labPage saveMixtureHavingName:self.saveTextField.text];

    
    
//    MixturesObject *mixture = [[[MixturesObject alloc]init]autorelease];
//    
//    mixture.name = self.saveTextField.text;
//    mixture.dateSaved = [NSDate date];
//    
//    ///convert self.views to mixtureObject format before saving the mixture in DB.
//    NSMutableArray *tempPoints = [NSMutableArray array];
//    NSMutableArray *tempImagesNames = [NSMutableArray array];
//    NSMutableArray *tempTypes = [NSMutableArray array];  
//    NSMutableArray *tempInertia = [NSMutableArray array];  
//    
//    for (SubstanceObject *substance in self.mainScene.views) {       
//        
//        [tempPoints addObject:[NSValue valueWithCGPoint:substance.center]];
//        [tempImagesNames addObject:substance.imageName];
//        [tempTypes addObject:substance.substanceType];
//        [tempInertia addObject:[NSNumber numberWithFloat:substance.inertia]];
//    }
//    
//    mixture.centerPoints = tempPoints;
//    mixture.imageNames = tempImagesNames;
//    mixture.types = tempTypes;
//    mixture.inertiaValues = tempInertia;
//    
//    [[PKObjectStore defaultObjectStore] storeObject: mixture callback: ^(BOOL success) {}];

    selectionHandler ();
}

- (void)viewDidUnload
{
    [self setSaveTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)dealloc {
  
    [selectionHandler release];
    [saveTextField release];
    [super dealloc];
}
@end
