//
//  MenuViewControlleriPad.m
//  iPower
//
//  Created by Daniel Moran on 5/26/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//
#import "QuartzCore/QuartzCore.h"
#import "MenuViewControlleriPad.h"
#import "SoundsController.h"
#import "PlaySounds.h"
#import "Constants.h"
#import "Appirater.h"
#import "AboutViewController.h"
#import "MainSceneViewController.h"

@interface MenuViewControlleriPad ()

@property (nonatomic, retain) MainSceneViewController *mainScene;

@end

@implementation MenuViewControlleriPad

@synthesize mainScene;
@synthesize shareBtn;
@synthesize rateAppBtn;
@synthesize aboutBtn;
@synthesize backgroundSwitch;
@synthesize gestureSwitch;
@synthesize consumeModeSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

+ (id) menu { 
    
    return [[[self alloc] init] autorelease]; 
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[self.shareBtn layer] setCornerRadius:8.0f];
    [[self.shareBtn layer] setMasksToBounds:YES];
    [[self.shareBtn layer] setBorderWidth:1.0f];
    
    [[self.rateAppBtn layer] setCornerRadius:8.0f];
    [[self.rateAppBtn layer] setMasksToBounds:YES];
    [[self.rateAppBtn layer] setBorderWidth:1.0f];
    
    [[self.aboutBtn layer] setCornerRadius:8.0f];
    [[self.aboutBtn layer] setMasksToBounds:YES];
    [[self.aboutBtn layer] setBorderWidth:1.0f];
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSUserDefaults *isOn = [NSUserDefaults standardUserDefaults];
    self.backgroundSwitch.on = [isOn boolForKey:kPlayBackgroundMusic];
    self.gestureSwitch.on = [isOn boolForKey:kPlayDraggingSound];
    self.consumeModeSwitch.on = [isOn boolForKey:kPlayConsumeSound];
    
}

- (void)viewDidUnload
{
    [self setBackgroundSwitch:nil];
    [self setGestureSwitch:nil];
    [self setConsumeModeSwitch:nil];
    [self setShareBtn:nil];
    [self setRateAppBtn:nil];
    [self setAboutBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)setBackgroundSound:(id)sender {
    
    BOOL isOn = [self.backgroundSwitch isOn];
    [SoundsController setBackgroundMusic:isOn];
}

- (IBAction)setGestureSound:(id)sender {
    
    BOOL isOn = [self.gestureSwitch isOn];
    [SoundsController setGestureSound:isOn];
}
- (IBAction)setConsumeModeSound:(id)sender {
    
    BOOL isOn = [self.consumeModeSwitch isOn];
    [SoundsController setConsumeSound:isOn];
}

-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
    
    self.mainScene = _mainSceneViewController;
}

- (IBAction)setBackgroundMarble:(id)sender {

    [self setBackgroundImageHaving:kBackgroundPad1];
}

- (IBAction)setBackgroundWood:(id)sender {
    
    [self setBackgroundImageHaving:kBackgroundPad2];
}

- (IBAction)setBackgroundLeather:(id)sender {
    
    [self setBackgroundImageHaving:kBackgroundPad3];
}

- (IBAction)setBackgroundMetal:(id)sender {
    
    [self setBackgroundImageHaving:kBackgroundPad4];
}

-(void) setBackgroundImageHaving:(NSString *) imageName {
     
    [[NSUserDefaults standardUserDefaults] setObject:imageName forKey:kBackgroundKeyName];
    UIImage *image = [UIImage imageNamed:imageName];
    [self.mainScene setBackgroundImageWithImage:image];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}
- (IBAction)emailAppInfo:(id)sender {
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
 
    if (mailClass != nil)
    {
    if ([mailClass canSendMail])
    {
    [self displayComposerSheet];
    }
    else
    {
    [self launchMailAppOnDevice];
    }
    }
    else
    {
    [self launchMailAppOnDevice];
    }
}

- (IBAction)rateApp:(id)sender {
    
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=492611634";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *reviewURL = templateReviewURL;
	[userDefaults setBool:YES forKey:kAppiraterRatedCurrentVersion];
	[userDefaults synchronize];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
    
}
- (IBAction)aboutApp:(id)sender {
    
    AboutViewController *about = [[[AboutViewController alloc]initWithNibName:@"AboutViewControlleriPad" bundle:nil]autorelease];
    [self presentModalViewController:about animated:YES];
    
}

-(void)launchMailAppOnDevice
{
    NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
    NSString *body = @"&body=It is raining in sunny California!";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

-(void)displayComposerSheet {
    
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    
    [mailComposer setSubject:@"Check out this iPhone app"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"logo114" ofType:@"png"];
    NSData *myData = [NSData dataWithContentsOfFile:path];
    [mailComposer addAttachmentData:myData mimeType:@"image/png" fileName:@"logo114"];
    
    NSString *emailBody = @"I found a really cool app in the iTunes store, check out iPower  <a href=\"http://itunes.apple.com/us/app/free-english-spanish-dictionary/id492611634?ls=1&mt=8\">Click Here</a>";
    [mailComposer setMessageBody:emailBody isHTML:YES];
    
    [self presentModalViewController:mailComposer animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    mailMessage.hidden = NO;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            mailMessage.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            mailMessage.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            mailMessage.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            mailMessage.text = @"Result: failed";
            break;
        default:
            mailMessage.text = @"Result: not sent";
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
    
}


- (void)dealloc {
    [backgroundSwitch release];
    [gestureSwitch release];
    [consumeModeSwitch release];
    [shareBtn release];
    [rateAppBtn release];
    [aboutBtn release];
    [super dealloc];
}
@end
