//
//  FlickitSHKConfigurator.m
//  iPower
//
//  Created by edward liveikis on 5/20/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "FlickitSHKConfigurator.h"

@implementation FlickitSHKConfigurator

#pragma mark - layout

// Name of the plist file that defines the class names of the sharers to use. Usually should not be changed, but 
// this allows you to subclass a sharer and have the subclass be used.
- (NSString*)sharersPlistName {
	return @"FlickitSHKSharers.plist";
}
// SHKActionSheet settings
- (NSNumber*)showActionSheetMoreButton {
	return [NSNumber numberWithBool:false];// Setting this to true will show More... button in SHKActionSheet, setting to false will leave the button out.
}

/*
 Favorite Sharers
 ----------------
 These values are used to define the default favorite sharers appearing on ShareKit's action sheet.
 */
- (NSArray*)defaultFavoriteURLSharers {
    return [NSArray arrayWithObjects:@"SHKFacebook", @"SHKTwitter", @"SHKMail", nil];
}
- (NSArray*)defaultFavoriteImageSharers {
    return [NSArray arrayWithObjects:@"SHKFacebook", @"SHKMail", @"SHKCopy", nil];
}
- (NSArray*)defaultFavoriteTextSharers {
    return [NSArray arrayWithObjects:@"SHKFacebook", @"SHKTwitter", @"SHKMail", nil];
}

//by default, user can see last used sharer on top of the SHKActionSheet. You can switch this off here, so that user is always presented the same sharers for each SHKShareType.
- (NSNumber*)autoOrderFavoriteSharers {
    return [NSNumber numberWithBool:false];
}

#pragma mark - general

- (NSString*)appName
{
    return @"Flickit";
}

- (NSString*)appURL {
	return @"http://tapcrew.com";
}

#pragma mark - facebook

- (NSString*)facebookAppId {
	return @"283028175127118";
}

#pragma mark - twitter

- (NSString*)twitterConsumerKey {
	return @"AI2DxqSvAqXb08NLGJW8g";
}

- (NSString*)twitterSecret {
	return @"RmvskIuLwUPLaUAdmZIpcsaLvWIlxhgt1v8tHSs";
}
// You need to set this if using OAuth, see note above (xAuth users can skip it)
- (NSString*)twitterCallbackUrl {
	return @"http://tapcrew.com/oauth";
}
// To use xAuth, set to 1
- (NSNumber*)twitterUseXAuth {
	return [NSNumber numberWithInt:0];
}
// Enter your app's twitter account if you'd like to ask the user to follow it when logging in. (Only for xAuth)
- (NSString*)twitterUsername {
	return @"tapcrew";
}

#pragma mark - user interface

// overridden so that we always show all options
- (NSNumber*)maxFavCount {
	return [NSNumber numberWithInt:20];
}

@end