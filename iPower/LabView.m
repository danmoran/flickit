//
//  LabView.m
//  iPower
//
//  Created by Daniel Moran on 4/2/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "LabView.h"
#import "MainSceneViewController.h"
#import "ContainerObject.h"
#import "Constants.h"

@interface LabView ()

@property (nonatomic, retain) MainSceneViewController *mainScene;
//@property (nonatomic, retain) UIView *backgroundView;
@property (nonatomic, retain) ContainerObject *containerObject;
//@property (nonatomic, retain) NSMutableArray *containerLevelValues;
@property (nonatomic, retain) UIView *popupView;
@property (nonatomic, retain) UILabel *slider1label, *slider2label, *slider3label, *slider4label;
@property (nonatomic, retain) UISlider *slider1;
@property (nonatomic, retain) UISlider *slider2;
@property (nonatomic, retain) UISlider *slider3;
@property (nonatomic, retain) UISlider *slider4;
@property (nonatomic, retain) UITextView *description;
@property (nonatomic, retain) UITextField *textbox;
@property (nonatomic, assign) CGFloat container1;
@property (nonatomic, assign) CGFloat container2;
@property (nonatomic, assign) CGFloat container3;
@property (nonatomic, assign) CGFloat container4;
@property (nonatomic, assign) BOOL is100Percent;

@end

@implementation LabView

@synthesize mainScene;
//@synthesize backgroundView;
@synthesize containerObject;
@synthesize popupView;
@synthesize slider1label, slider2label, slider3label, slider4label;
@synthesize slider1, slider2, slider3, slider4;
@synthesize container1, container2, container3, container4;
@synthesize description, textbox;
@synthesize is100Percent;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self loadInterface];
    }
    return self;
}



-(void) loadInterface {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        UIView *backgroundView = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)]autorelease];
        UIImage *image = [UIImage imageNamed:@"labPageBackground.png"];
        backgroundView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        
        [self addSubview:backgroundView];
        backgroundView.alpha = 0.0f;
        
        [UIView animateWithDuration: 0.3 animations: ^{
            
            backgroundView.alpha = 1.0f;
        }];
        
        self.slider1label = [[[UILabel alloc]initWithFrame:CGRectMake(30, 65, 100, 15)]autorelease];
        self.slider1label.backgroundColor = [UIColor clearColor];
        self.slider1label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider1label];
        
        self.slider2label = [[[UILabel alloc]initWithFrame:CGRectMake(110, 65, 100, 15)]autorelease];
        self.slider2label.backgroundColor = [UIColor clearColor];
        self.slider2label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider2label];
        
        self.slider3label = [[[UILabel alloc]initWithFrame:CGRectMake(180, 65, 100, 15)]autorelease];
        self.slider3label.backgroundColor = [UIColor clearColor];
        self.slider3label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider3label];
        
        self.slider4label = [[[UILabel alloc]initWithFrame:CGRectMake(250, 65, 100, 15)]autorelease];
        self.slider4label.backgroundColor = [UIColor clearColor];
        self.slider4label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider4label];
        
        
        UIImage *minImage = [UIImage imageNamed:@"sliderBarWhite.png"];
        UIImage *maxImage = [UIImage imageNamed:@"sliderBarEmpty.png"];
        UIImage *tumbImage = [UIImage imageNamed:@"sliderWhiteBtn.png"];
        /// trans must be -0.5 in order for min value to be at bottom
        CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
        self.slider1 = [[[UISlider alloc]initWithFrame:CGRectMake(-55, 140, 200, 80)]autorelease];
        [self.slider1 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider1 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider1 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider1 addTarget:self action:@selector(slider1Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider1.transform = trans;
        [backgroundView addSubview:self.slider1];
        self.slider1.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarGreen.png"];
        tumbImage = [UIImage imageNamed:@"sliderGreenBtn.png"]; 
        self.slider2 = [[[UISlider alloc]initWithFrame:CGRectMake(20, 140, 200, 80)]autorelease];
        [self.slider2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider2 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider2 addTarget:self action:@selector(slider2Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider2.transform = trans;
        [backgroundView addSubview:self.slider2];
        self.slider2.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarBlue.png"];
        tumbImage = [UIImage imageNamed:@"sliderBlueBtn.png"];
        self.slider3 = [[[UISlider alloc]initWithFrame:CGRectMake(90, 140, 200, 80)]autorelease];
        [self.slider3 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider3 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider3 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider3 addTarget:self action:@selector(slider3Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider3.transform = trans;
        [backgroundView addSubview:self.slider3];
        self.slider3.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarPink.png"];
        tumbImage = [UIImage imageNamed:@"sliderPinkBtn.png"];
        self.slider4 = [[[UISlider alloc]initWithFrame:CGRectMake(160, 140, 200, 80)]autorelease];
        [self.slider4 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider4 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider4 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider4 addTarget:self action:@selector(slider4Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider4.transform = trans;
        [backgroundView addSubview:self.slider4];
        self.slider4.continuous = NO;
        
        
        self.description = [[[UITextView alloc]initWithFrame:CGRectMake(15, 300, 260, 60)]autorelease];
        self.description.text = kContainerDescription1;
        self.description.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        self.description.editable = NO;
        self.description.backgroundColor = [UIColor clearColor];
        [backgroundView addSubview:self.description];
        
        
        UILabel *labelMenu = [[[UILabel alloc]initWithFrame:CGRectMake(-5, 460, 100, 15)]autorelease];
        labelMenu.backgroundColor = [UIColor clearColor];
        labelMenu.textAlignment = UITextAlignmentCenter;
        labelMenu.text = @"Menu";
        labelMenu.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelMenu];
        
        UILabel *labelSave = [[[UILabel alloc]initWithFrame:CGRectMake(75, 460, 100, 15)]autorelease];
        labelSave.backgroundColor = [UIColor clearColor];
        labelSave.textAlignment = UITextAlignmentCenter;
        labelSave.text = @"Save";
        labelSave.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelSave];
        
        UILabel *labelList = [[[UILabel alloc]initWithFrame:CGRectMake(150, 460, 100, 15)]autorelease];
        labelList.backgroundColor = [UIColor clearColor];
        labelList.textAlignment = UITextAlignmentCenter;
        labelList.text = @"Mixtures List";
        labelList.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelList];
        
        UILabel *labelClose = [[[UILabel alloc]initWithFrame:CGRectMake(230, 460, 100, 15)]autorelease];
        labelClose.backgroundColor = [UIColor clearColor];
        labelClose.textAlignment = UITextAlignmentCenter;
        labelClose.text = @"Close";
        labelClose.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelClose];
        
        UIButton *menu = [UIButton buttonWithType:UIButtonTypeCustom];
        menu.frame = CGRectMake(15, 400, 60, 60);
        UIImage *menuImage = [UIImage imageNamed:@"menu.png"];
        [menu setImage:menuImage forState:UIControlStateNormal];
        [menu addTarget:self action:@selector(presentMenuViewController)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: menu];
        
        UIButton *save = [UIButton buttonWithType:UIButtonTypeCustom];
        save.frame = CGRectMake(95, 400, 60, 60);
        UIImage *saveImage = [UIImage imageNamed:@"save.png"];
        [save setImage:saveImage forState:UIControlStateNormal];
        [save addTarget:self action:@selector(presentSavePopupView)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: save];
        
        UIButton *mixtureList = [UIButton buttonWithType:UIButtonTypeCustom];
        mixtureList.frame = CGRectMake(170, 400, 60, 60);
        UIImage *mixtureImage = [UIImage imageNamed:@"list.png"];
        [mixtureList setImage:mixtureImage forState:UIControlStateNormal];
        [mixtureList addTarget:self action:@selector(presentSavedMixturesViewController)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: mixtureList];
        
        UIButton *play = [UIButton buttonWithType:UIButtonTypeCustom];
        play.frame = CGRectMake(245, 400, 60, 60);
        UIImage *playImage = [UIImage imageNamed:@"close.png"];
        [play setImage:playImage forState:UIControlStateNormal];
        [play addTarget:self action:@selector(removeViewFromMainScene)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: play];

        
    } else {
        
        UIImage *image = [UIImage imageNamed:@"labPageBackgroundPad.png"];
        UIImageView *backgroundView = [[[UIImageView alloc]initWithImage:image]autorelease];
        backgroundView.userInteractionEnabled = YES;

        [self addSubview:backgroundView];
        backgroundView.alpha = 0.0f;
        
        [UIView animateWithDuration: 0.3 animations: ^{
            
            backgroundView.alpha = 1.0f;
        }];
        
        self.slider1label = [[[UILabel alloc]initWithFrame:CGRectMake(100, 115, 240, 15)]autorelease];
        self.slider1label.backgroundColor = [UIColor clearColor];
        self.slider1label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider1label];
        
        self.slider2label = [[[UILabel alloc]initWithFrame:CGRectMake(195, 115, 240, 15)]autorelease];
        self.slider2label.backgroundColor = [UIColor clearColor];
        self.slider2label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider2label];
        
        self.slider3label = [[[UILabel alloc]initWithFrame:CGRectMake(285, 115, 100, 15)]autorelease];
        self.slider3label.backgroundColor = [UIColor clearColor];
        self.slider3label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider3label];
        
        self.slider4label = [[[UILabel alloc]initWithFrame:CGRectMake(380, 115, 100, 15)]autorelease];
        self.slider4label.backgroundColor = [UIColor clearColor];
        self.slider4label.font = [UIFont fontWithName:@"Helvetica" size:12];
        [backgroundView addSubview:self.slider4label];
        
        
        UIImage *minImage = [UIImage imageNamed:@"sliderBarWhitePad.png"];
        UIImage *maxImage = [UIImage imageNamed:@"sliderBarEmptyPad.png"];
        UIImage *tumbImage = [UIImage imageNamed:@"sliderWhiteBtnPad.png"];
        /// trans must be -0.5 in order for min value to be at bottom
        CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
        self.slider1 = [[[UISlider alloc]initWithFrame:CGRectMake(-10, 220, 250, 80)]autorelease];
        [self.slider1 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider1 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider1 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider1 addTarget:self action:@selector(slider1Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider1.transform = trans;
        [backgroundView addSubview:self.slider1];
        self.slider1.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarGreenPad.png"];
        tumbImage = [UIImage imageNamed:@"sliderGreenBtnPad.png"]; 
        self.slider2 = [[[UISlider alloc]initWithFrame:CGRectMake(80, 220, 250, 80)]autorelease];
        [self.slider2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider2 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider2 addTarget:self action:@selector(slider2Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider2.transform = trans;
        [backgroundView addSubview:self.slider2];
        self.slider2.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarBluePad.png"];
        tumbImage = [UIImage imageNamed:@"sliderBlueBtnPad.png"];
        self.slider3 = [[[UISlider alloc]initWithFrame:CGRectMake(170, 220, 250, 80)]autorelease];
        [self.slider3 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider3 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider3 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider3 addTarget:self action:@selector(slider3Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider3.transform = trans;
        [backgroundView addSubview:self.slider3];
        self.slider3.continuous = NO;
        
        minImage = [UIImage imageNamed:@"sliderBarPinkPad.png"];
        tumbImage = [UIImage imageNamed:@"sliderPinkBtnPad.png"];
        self.slider4 = [[[UISlider alloc]initWithFrame:CGRectMake(260, 220, 250, 80)]autorelease];
        [self.slider4 setMinimumTrackImage:minImage forState:UIControlStateNormal];
        [self.slider4 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
        [self.slider4 setThumbImage:tumbImage forState:UIControlStateNormal];
        [self.slider4 addTarget:self action:@selector(slider4Changed) 
               forControlEvents:UIControlEventValueChanged];
        self.slider4.transform = trans;
        [backgroundView addSubview:self.slider4];
        self.slider4.continuous = NO;
        
        self.description = [[[UITextView alloc]initWithFrame:CGRectMake(40, 390, 260, 60)]autorelease];
        self.description.text = kContainerDescription1;
        self.description.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        self.description.editable = NO;
        self.description.backgroundColor = [UIColor clearColor];
        [backgroundView addSubview:self.description];
        
        UILabel *labelSave = [[[UILabel alloc]initWithFrame:CGRectMake(65, 585, 100, 15)]autorelease];
        labelSave.backgroundColor = [UIColor clearColor];
        labelSave.textAlignment = UITextAlignmentCenter;
        labelSave.text = @"Save";
        labelSave.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelSave];
        
        UILabel *labelList = [[[UILabel alloc]initWithFrame:CGRectMake(195, 585, 100, 15)]autorelease];
        labelList.backgroundColor = [UIColor clearColor];
        labelList.textAlignment = UITextAlignmentCenter;
        labelList.text = @"Mixtures List";
        labelList.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelList];
        
        UILabel *labelClose = [[[UILabel alloc]initWithFrame:CGRectMake(325, 585, 100, 15)]autorelease];
        labelClose.backgroundColor = [UIColor clearColor];
        labelClose.textAlignment = UITextAlignmentCenter;
        labelClose.text = @"Close";
        labelClose.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
        [backgroundView addSubview:labelClose];
        
        UIButton *save = [UIButton buttonWithType:UIButtonTypeCustom];
        save.frame = CGRectMake(70, 480, 94, 94);
        UIImage *saveImage = [UIImage imageNamed:@"savePad.png"];
        [save setImage:saveImage forState:UIControlStateNormal];
        [save addTarget:self action:@selector(presentSavePopupView)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: save];
        
        UIButton *mixtureList = [UIButton buttonWithType:UIButtonTypeCustom];
        mixtureList.frame = CGRectMake(200, 480, 94, 94);
        UIImage *mixtureImage = [UIImage imageNamed:@"listPad.png"];
        [mixtureList setImage:mixtureImage forState:UIControlStateNormal];
        [mixtureList addTarget:self action:@selector(presentSavedMixturesViewController)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: mixtureList];
        
        UIButton *play = [UIButton buttonWithType:UIButtonTypeCustom];
        play.frame = CGRectMake(330, 480, 94, 94);
        UIImage *playImage = [UIImage imageNamed:@"closePad.png"];
        [play setImage:playImage forState:UIControlStateNormal];
        [play addTarget:self action:@selector(removeViewFromMainScene)forControlEvents:UIControlEventTouchDown];
        [backgroundView addSubview: play];
        
    }
}

-(void) presentSavePopupView {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        

        self.popupView = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 460)]autorelease];
        UIImage *image = [UIImage imageNamed:@"popup.png"];
        self.popupView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        self.textbox = [[[UITextField alloc]initWithFrame:CGRectMake(65, 120, 190, 25)]autorelease];
        self.textbox.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        self.textbox.backgroundColor = [UIColor whiteColor];
        [self.popupView addSubview:self.textbox];
        [textbox becomeFirstResponder];
        
        UILabel *label = [[[UILabel alloc]initWithFrame:CGRectMake(55, 80, 180, 30)]autorelease];
        label.text = @"Enter Mixture Name";
        label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        label.backgroundColor = [UIColor clearColor];
        [self.popupView addSubview:label];
        
        UIButton *ok = [UIButton buttonWithType:UIButtonTypeCustom];
        ok.frame = CGRectMake(195, 165, 60, 60);
        UIImage *okImage = [UIImage imageNamed:@"ok.png"];
        [ok setImage:okImage forState:UIControlStateNormal];
        [ok addTarget:self action:@selector(saveMixture)forControlEvents:UIControlEventTouchDown];
        [self.popupView addSubview: ok];
        
        UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
        cancel.frame = CGRectMake(65, 165, 60, 60);
        UIImage *cancelImage = [UIImage imageNamed:@"cancel.png"];
        [cancel setImage:cancelImage forState:UIControlStateNormal];
        [cancel addTarget:self action:@selector(cancelSave)forControlEvents:UIControlEventTouchDown];
        [self.popupView addSubview: cancel];
        
        [self addSubview:popupView];
        
    } else {
        
        self.popupView = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, 500, 350)]autorelease];
        UIImage *image = [UIImage imageNamed:@"saveDialogPad.png"];
        self.popupView.backgroundColor = [UIColor colorWithPatternImage:image];
        
        self.textbox = [[[UITextField alloc]initWithFrame:CGRectMake(65, 120, 190, 40)]autorelease];
        self.textbox.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16];
        self.textbox.backgroundColor = [UIColor whiteColor];
        [self.popupView addSubview:self.textbox];
        [textbox becomeFirstResponder];
        
        UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
        cancel.frame = CGRectMake(100, 210, 94, 94);
        UIImage *cancelImage = [UIImage imageNamed:@"cancelPad.png"];
        [cancel setImage:cancelImage forState:UIControlStateNormal];
        [cancel addTarget:self action:@selector(cancelSave)forControlEvents:UIControlEventTouchDown];
        [self.popupView addSubview: cancel];
        
        UIButton *ok = [UIButton buttonWithType:UIButtonTypeCustom];
        ok.frame = CGRectMake(310, 210, 94, 94);
        UIImage *okImage = [UIImage imageNamed:@"okPad.png"];
        [ok setImage:okImage forState:UIControlStateNormal];
        [ok addTarget:self action:@selector(saveMixture)forControlEvents:UIControlEventTouchDown];
        [self.popupView addSubview: ok];
        
        [self addSubview:popupView];
        
    }
}

-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
    
    self.mainScene = _mainSceneViewController;
}

-(void) removeViewFromMainScene {

    [UIView animateWithDuration: 0.3 animations: ^{
        self.alpha = 0.0f;
    }];
    
    [self.mainScene presentLabView:nil];
}

-(void) presentSavedMixturesViewController {

    [self.mainScene presentSavedMixturesViewController];
//    self.mainScene.shouldPlayBackgroundMusic = NO;
    [self removeViewFromMainScene];
}

-(void) presentMenuViewController {
    
    [UIView animateWithDuration: 0.3 animations: ^{
        self.alpha = 0.0f;
    }];

    [self.mainScene presentMenuViewController];
}

-(void) saveMixture {
    
    if (![self.textbox.text isEqualToString:@""]) {
        
        [self.mainScene saveMixtureInDBWithName:textbox.text];    
    }
    [popupView removeFromSuperview];
}

-(void) cancelSave {
    
    [popupView removeFromSuperview];
}
-(void) slider1Changed {
    
    self.description.text = kContainerDescription1;
    [self updateSliders:kContainer1]; 
    
}
-(void) slider2Changed { 
    
    self.description.text = kContainerDescription2;    
    [self updateSliders:kContainer2]; 
}
-(void) slider3Changed { 
    
    self.description.text = kContainerDescription3; 
    [self updateSliders:kContainer3]; 
}
-(void) slider4Changed { 
    
    self.description.text = kContainerDescription4; 
    [self updateSliders:kContainer4]; 
}

-(void) updateSliders:(NSString *) container {

    CGFloat differenceInValue = 0;
    CGFloat sumContainer;
    CGFloat tempContainerValue1 = 0.0f;
    CGFloat tempContainerValue2 = 0.0f;
    CGFloat tempContainerValue3 = 0.0f;
    CGFloat tempContainerValue4 = 0.0f;
    CGFloat sumContainers = slider1.value + slider2.value + slider3.value + slider4.value;
    
    if (sumContainers > 1) {
        
        if (container == kContainer1) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider1.value - container1;
            }
            sumContainer = container2 + container3 + container4;
            
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }      
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }      
            if (differenceInValue>0) {
                container2 = container2 - tempContainerValue2;
                container3 = container3 - tempContainerValue3;
                container4 = container4 - tempContainerValue4;
            }            
            container1  = slider1.value;
            slider2.value = container2;
            slider3.value = container3;
            slider4.value = container4;
            
            [self updateContainerLabels];

            
        } else if (container == kContainer2) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider2.value - container2;
            }
            sumContainer = container1 + container3 + container4;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container3 = container3 - tempContainerValue3;
                container4 = container4 - tempContainerValue4;
            }
            container2  = slider2.value;
            slider1.value = container1;
            slider3.value = container3;
            slider4.value = container4;
            
            [self updateContainerLabels];
            
        } else if (container == kContainer3) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider3.value - container3;
            }
            sumContainer = container1 + container2 + container4;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container2 = container2 - tempContainerValue2;
                container4 = container4 - tempContainerValue4;
            }
            container3  = slider3.value;
            slider1.value = container1;
            slider2.value = container2;
            slider4.value = container4;
            
            [self updateContainerLabels];
            
        }else if (container == kContainer4) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider4.value - container4;
            }
            sumContainer = container1 + container2 + container3;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container2 = container2 - tempContainerValue2;
                container3 = container3 - tempContainerValue3;
            }
            container4  = slider4.value;
            slider1.value = container1;
            slider2.value = container2;
            slider3.value = container3;
            
            [self updateContainerLabels];
        }


    }else {
        is100Percent = NO;
        container1 = slider1.value;
        container2 = slider2.value;
        container3 = slider3.value;
        container4 = slider4.value;
        
        [self updateContainerLabels];
    }
    
    NSMutableDictionary *tempContainerValues = [NSMutableDictionary dictionary];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider1.value] forKey:kContainer1];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider2.value] forKey:kContainer2];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider3.value] forKey:kContainer3];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider4.value] forKey:kContainer4];
    
    [self.mainScene updateSubstanceTypesOnScreenWithValuesIn:tempContainerValues];
}
-(void) passValuesToSlidersFromDictionary:(NSDictionary *)containerValues {
   
    slider1.value = [[containerValues objectForKey:kContainer1] floatValue];
    slider2.value = [[containerValues objectForKey:kContainer2] floatValue];
    slider3.value = [[containerValues objectForKey:kContainer3] floatValue];
    slider4.value = [[containerValues objectForKey:kContainer4] floatValue];    
    
    container1 = slider1.value;
    container2 = slider2.value;
    container3 = slider3.value;
    container4 = slider4.value;

    [self updateContainerLabels];
    
}
-(void) updateContainerLabels {
    
    self.slider1label.text = [NSString stringWithFormat:@"%.0f %%", slider1.value * 100];
    self.slider2label.text = [NSString stringWithFormat:@"%.0f %%", slider2.value * 100];
    self.slider3label.text = [NSString stringWithFormat:@"%.0f %%", slider3.value * 100];
    self.slider4label.text = [NSString stringWithFormat:@"%.0f %%", slider4.value * 100];
}

- (void)dealloc {

    [mainScene release];
    [super dealloc];
}
@end
