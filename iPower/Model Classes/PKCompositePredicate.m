//
//  PKCompositePredicate.m
//  PKTest
//
//  Created by Petr Homola on 6/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PKCompositePredicate.h"

@implementation PKCompositePredicate

- (id)initWithComposition:(PKPredicateComposition)_composition {
	if ((self = [super init])) {
		composition = _composition;
		predicates = [[NSMutableArray alloc] init];
	}
	return self;
}

- (BOOL)matchesObject:(id<PKPersistentObject>)object {
	for (PKPredicate* predicate in predicates) {
		BOOL matches = [predicate matchesObject: object];
		if (matches == YES && composition == kPKPredicateCompositionOr) return YES;
		if (matches == NO && composition == kPKPredicateCompositionAnd) return NO;
	}
	return composition == kPKPredicateCompositionAnd;
}

- (void)addPredicate:(PKPredicate*)predicate {
	[predicates addObject: predicate];
}


@end
