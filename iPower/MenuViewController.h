//
//  MenuViewController.h
//  iPower
//
//  Created by Daniel Moran on 4/4/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MenuViewController : UIViewController <MFMailComposeViewControllerDelegate>
{

    UILabel *mailMessage;
}



@property (retain, nonatomic) IBOutlet UIButton *playBtn;
@property (retain, nonatomic) IBOutlet UIButton *backgroundBtn;
@property (retain, nonatomic) IBOutlet UIButton *shareBtn;
@property (retain, nonatomic) IBOutlet UIButton *rateAppBtn;
@property (retain, nonatomic) IBOutlet UIButton *soundBtn;
@property (retain, nonatomic) IBOutlet UIButton *aboutBtn;



-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;


@end
