//
//  PKCompositePredicate.h
//  PKTest
//
//  Created by Petr Homola on 6/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKPredicate.h"

typedef enum {
	kPKPredicateCompositionAnd,
	kPKPredicateCompositionOr
} PKPredicateComposition;

@interface PKCompositePredicate : PKPredicate {
	PKPredicateComposition composition;
	NSMutableArray* predicates;
}

- (id)initWithComposition:(PKPredicateComposition)_composition;
- (void)addPredicate:(PKPredicate*)predicate;

@end
