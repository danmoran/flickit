//
//  SubstanceObject.m
//  iPower
//
//  Created by daniel moran on 3/6/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "SubstanceObject.h"
#import "Constants.h"
#import "MainSceneViewController.h"
@implementation SubstanceObject

//@synthesize substanceView;
@synthesize centerPoint;
@synthesize imageName, substanceType;
//@synthesize inertia;
@synthesize startingAtCount;

@synthesize mainScene;


//- (id)initWithCoder:(NSCoder *)coder {
//    self = [super initWithCoder:coder];
//
//    return self;
//}
//
//- (void)encodeWithCoder:(NSCoder *)coder {
//    [super encodeWithCoder:coder];
//    
//    
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, 9, 9);
    }
    return self;
}
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
    
    self.mainScene = _mainSceneViewController;
}

-(CGPoint) viewsCenterPointWith:(CGFloat)xValue andYValue:(CGFloat)yValue {

    centerPoint = CGPointMake(xValue, yValue);
    
    return centerPoint;
}

//-(NSMutableArray *) test {
//    
//    NSMutableArray *myArray = [NSMutableArray array];
//    [myArray addObject:@"one"];
//    return myArray;
//}

//-(NSMutableArray *) loadSubstanceObjectsWithMaxDist:(int) dist1 minDist:(int)dist2 startingCountAt:(int) startingCount randomlocation:(BOOL) useRandomPlacement forView:(UIView *)view {
//    
//    NSMutableArray *substanceObjects = [[NSMutableArray alloc]init];
//    
//    CGFloat w = view.frame.size.width;
//    CGFloat h = view.frame.size.height;
//    CGFloat xMinPosibleVal = w*.30;
//    CGFloat xMaxPosibleVal = w - (w*.30);
//    CGFloat yMaxPosibleVal = h;
//    CGFloat xCenterPoint = w/2;
//    CGFloat yCenterPoint = h/2;
//    CGFloat x = xCenterPoint - 100;
//    CGFloat y  = yCenterPoint - 100;
//    
//    int numberOfSubstanceObjects = kNumberOfSubtanceObjects;
//    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) numberOfSubstanceObjects = kNumberOfSubtanceObjects *kPadMultiplier;
//    
//    for (int i = startingCount; i < numberOfSubstanceObjects; i++) {
//        
//        self.startingAtCount = i;
//        
//        int randomImageNumber = arc4random()%8;
//        if (useRandomPlacement) randomImageNumber = (arc4random()%(12-7))+7; //range from 7 to 12
//        
//        randomImageNumber++;
//        NSMutableString *randomImageName = [NSMutableString stringWithString:@"substanceWhite"];
//        [randomImageName appendString:[NSString stringWithFormat:@"%i", randomImageNumber]];
//        UIImage *image = [UIImage imageNamed:imageName];
//        
//        CGPoint centerView = [self viewsCenterPointWith:x andYValue:y];        
//        CGPoint screenCenterPoint = CGPointMake(xCenterPoint, yCenterPoint);
//        CGFloat dx = screenCenterPoint.x - centerView.x;
//        CGFloat dy = screenCenterPoint.y - centerView.y;
//        CGFloat distance = sqrtf(dx*dx + dy*dy);
//        //NOTE: 40 works for iPhone
//        if (distance < dist1 && distance > dist2) {
//            
//            self.image = image;
//            self.imageName = imageName;
//            self.substanceType = kContainer1;
//            self.inertia = kMinInertiaValue;
//            [self setCenter:centerView];
////            [view addSubview:self];        
//            [substanceObjects addObject: self];
//            [self.mainScene.view addSubview:self];
//        }
//        else {
//            i--;
//            
//            if (y>yMaxPosibleVal) {
//                
//                return substanceObjects;
//                break;
//                
//            }
//        }
//        
//        if (useRandomPlacement) {
//            int xRandomLocation = arc4random()%30;
//            
//            x = x + kObjectSize + xRandomLocation;
//            
//            if (x > xMaxPosibleVal) {
//                x = xMinPosibleVal;
//                y = y + kObjectSize + xRandomLocation;
//            }
//            
//        }else {
//            x = x + kObjectSize -4;
//            
//            if (x > xMaxPosibleVal) {
//                x = xMinPosibleVal;
//                y = y + kObjectSize -4;
//            }
//        }
//    }
//    return substanceObjects;
//}


- (void)dealloc {

    [super dealloc];
}
@end
