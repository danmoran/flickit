//
//  ContainerObject.m
//  iPower
//
//  Created by Daniel Moran on 4/13/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "ContainerObject.h"
#import "MixturesObject.h"
#import "Constants.h"
#import "SubstanceObject.h"
#import "DeviceTypes.h"

@implementation ContainerObject

@synthesize containers;

- (id)init {
	if ((self = [super init])) {
        
	}
	return self;
}
+ (id)container { 
    
    return [[[self alloc] init] autorelease]; 
}

-(NSDictionary *) updateContainerlevelsUsingMixtureObject:(NSMutableArray *)substances {
    
    CGFloat countsContainer1 = 0;
    CGFloat countsContainer2 = 0;
    CGFloat countsContainer3 = 0;
    CGFloat countsContainer4 = 0;
    
    CGFloat container1Value = 0;
    CGFloat container2Value = 0;
    CGFloat container3Value = 0;
    CGFloat container4Value = 0;
    
    
    for (SubstanceObject *substance in substances) {

        if ([substance.substanceType isEqualToString:kContainer1]) {
            countsContainer1++;
        }else if ([substance.substanceType isEqualToString:kContainer2]) {
            countsContainer2++;
        }else if ([substance.substanceType isEqualToString:kContainer3]) {
            countsContainer3++;
        }else if ([substance.substanceType isEqualToString:kContainer4]) {
            countsContainer4++;
        }
    }
    
    if ([substances count]>0) {
        CGFloat substaceCountMultiplier = [DeviceTypes determineDeviceTypeMultiplier];
        
        CGFloat numberOfSubstanceObjects = kNumberOfSubtanceObjects * substaceCountMultiplier;
//        TODO: the if statement below is not necessary...
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) numberOfSubstanceObjects = kNumberOfSubtanceObjects *substaceCountMultiplier;

        
        container1Value = countsContainer1>0 ? countsContainer1/numberOfSubstanceObjects : 0;
        container2Value = countsContainer2>0 ? countsContainer2/numberOfSubstanceObjects : 0;
        container3Value = countsContainer3>0 ? countsContainer3/numberOfSubstanceObjects : 0;
        container4Value = countsContainer4>0 ? countsContainer4/numberOfSubstanceObjects : 0;
    }
    
    
    NSArray *values = [NSArray arrayWithObjects:[NSNumber numberWithDouble:container1Value], [NSNumber numberWithDouble:container2Value], [NSNumber numberWithDouble:container3Value], [NSNumber numberWithDouble:container4Value], nil];
    
    NSArray *tempContainers = [NSArray arrayWithObjects:kContainer1, kContainer2, kContainer3, kContainer4, nil];
    
    self.containers = [NSDictionary dictionaryWithObjects:values forKeys:tempContainers];
    
    return self.containers;
}

-(NSMutableArray *) udpateMixtureSubstanceTypesUsing:(NSDictionary *) containerLevels forSubstances:(NSMutableArray *) substances {
    
    CGFloat substaceCountMultiplier = [DeviceTypes determineDeviceTypeMultiplier];
    
    CGFloat numberOfSubstanceObjects = kNumberOfSubtanceObjects * substaceCountMultiplier;
    //        TODO: the if statement below is not necessary...
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) numberOfSubstanceObjects = kNumberOfSubtanceObjects *substaceCountMultiplier;
    
    NSInteger containerValue1 = [[containerLevels objectForKey:kContainer1] floatValue]*numberOfSubstanceObjects;
    NSInteger containerValue2 = containerValue1 + [[containerLevels objectForKey:kContainer2] floatValue]*numberOfSubstanceObjects;
    NSInteger containerValue3 = containerValue2 + [[containerLevels objectForKey:kContainer3] floatValue]*numberOfSubstanceObjects;
    NSInteger containerValue4 = containerValue3 + [[containerLevels objectForKey:kContainer4] floatValue]*numberOfSubstanceObjects;

    NSInteger remainingSubstance = containerValue4;
    
    int i = 0;
    for (i = 0; [substances count]>i; i++) {
        SubstanceObject *substance = [substances objectAtIndex:i];
        
        if (i<containerValue1) {
            substance.substanceType = kContainer1;
            
            NSMutableString *imageName = [NSMutableString stringWithString:@"substanceWhite"];
            [self setImageForSubstanceObject:substance havingImageName:imageName];
        } 
        else if (i>=containerValue1 && i<containerValue2) {
            substance.substanceType = kContainer2;
            
            NSMutableString *imageName = [NSMutableString stringWithString:@"substanceGreen"];
            [self setImageForSubstanceObject:substance havingImageName:imageName];
        } 
        else if (i>=containerValue2 && i<containerValue3) {
            substance.substanceType = kContainer3;       
            
            NSMutableString *imageName = [NSMutableString stringWithString:@"substanceBlue"];
            [self setImageForSubstanceObject:substance havingImageName:imageName];
            
        } else if (i>=containerValue3 && i<=containerValue4) {
            substance.substanceType = kContainer4;
            
            NSMutableString *imageName = [NSMutableString stringWithString:@"substancePink"];
            [self setImageForSubstanceObject:substance havingImageName:imageName];

        } 
        else if(i>=remainingSubstance) { 
            substance.hidden = YES;
            substance.substanceType = kContainerx;
        }
    }
    return substances;
}

-(void) setImageForSubstanceObject:(SubstanceObject *)substance havingImageName :(NSMutableString *) imageName {
    
    int randomImageNumber = arc4random()%9;
    randomImageNumber  = randomImageNumber +1;
    
    [imageName appendString:[NSString stringWithFormat:@"%i.png", randomImageNumber]];
    
    substance.image = [UIImage imageNamed:imageName];
    substance.imageName = imageName;
    substance.hidden = NO;
}


@end
