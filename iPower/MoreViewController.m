//
//  MoreViewController.m
//  iPower
//
//  Created by Daniel Moran on 2/22/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "MoreViewController.h"
#import "AboutViewController.h"
#import "Appirater.h"

#import "FlickitSharing.h"

@implementation MoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)presentSettingsViewController:(id)sender {

    [self dismissModalViewControllerAnimated:NO];
}

- (void)shareApp
{
    if (self.view)
    {
        [FlickitSharing appShareGeneralInfoFromView:self.view];
    }
}

- (IBAction)presentTellOthers:(id)sender {
    [self shareApp];
    
//    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//    if (mailClass != nil)
//    {
//        
//        if ([mailClass canSendMail])
//        {
//            [self displayComposerSheet];
//        }
//        else
//        {
//            [self launchMailAppOnDevice];
//        }
//    }
//    else
//    {
//        [self launchMailAppOnDevice];
//    }

}

- (IBAction)upgradeToFullVersion:(id)sender {
}

- (IBAction)rateApp:(id)sender {
    
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=492611634";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *reviewURL = templateReviewURL;
	[userDefaults setBool:YES forKey:kAppiraterRatedCurrentVersion];
	[userDefaults synchronize];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];

}

- (IBAction)presentAboutViewController:(id)sender {
    
    AboutViewController *about = [[[AboutViewController alloc]init]autorelease];
    [self presentModalViewController:about animated:NO];
}

-(void)launchMailAppOnDevice
{
    NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
    NSString *body = @"&body=It is raining in sunny California!";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

#pragma mark Compose Mail

-(void)displayComposerSheet {
    
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    
    [mailComposer setSubject:@"Check out this iPhone app"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"logo114" ofType:@"png"];
    NSData *myData = [NSData dataWithContentsOfFile:path];
    [mailComposer addAttachmentData:myData mimeType:@"image/png" fileName:@"logo114"];
    
    NSString *emailBody = @"I found a really cool app in the iTunes store, check out iPower  <a href=\"http://itunes.apple.com/us/app/free-english-spanish-dictionary/id492611634?ls=1&mt=8\">Click Here</a>";
    [mailComposer setMessageBody:emailBody isHTML:YES];
    
    [self presentModalViewController:mailComposer animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    mailMessage.hidden = NO;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            mailMessage.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            mailMessage.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            mailMessage.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            mailMessage.text = @"Result: failed";
            break;
        default:
            mailMessage.text = @"Result: not sent";
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{

    [tellOthersBtn release];
    tellOthersBtn = nil;
    [upgradeToFullVersionBtn release];
    upgradeToFullVersionBtn = nil;
    [rateAppBtn release];
    rateAppBtn = nil;
    [aboutBtn release];
    aboutBtn = nil;
    [menuBtn release];
    menuBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [tellOthersBtn release];
    [upgradeToFullVersionBtn release];
    [rateAppBtn release];
    [aboutBtn release];
    [menuBtn release];
    [super dealloc];
}
@end
