//
//  SoundsController.m
//  iPower
//
//  Created by Daniel Moran on 6/1/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "SoundsController.h"
#import "Constants.h"

@implementation SoundsController

+ (void) setBackgroundMusic:(BOOL) setOn {
    
    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
    
    if (setOn) {
        
        [shouldPlaySound setBool:YES forKey:kPlayBackgroundMusic];
    } else {
        [shouldPlaySound setBool:NO forKey:kPlayBackgroundMusic];
    }
}

+ (void) setGestureSound:(BOOL) setOn {
    
    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
    
    if (setOn) {
        
        [shouldPlaySound setBool:YES forKey:kPlayDraggingSound];
    } else {
        [shouldPlaySound setBool:NO forKey:kPlayDraggingSound];
    }
}

+ (void) setConsumeSound:(BOOL) setOn {
    
    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
    
    if (setOn) {
        
        [shouldPlaySound setBool:YES forKey:kPlayConsumeSound];
    } else {
        [shouldPlaySound setBool:NO forKey:kPlayConsumeSound];
    }
}

@end
