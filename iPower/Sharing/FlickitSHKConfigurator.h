//
//  FlickitSHKConfigurator.h
//  iPower
//
//  Created by edward liveikis on 5/20/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "ShareKitDemoConfigurator.h"

@interface FlickitSHKConfigurator : DefaultSHKConfigurator


// general
- (NSString*)appName;
- (NSString*)appURL;

// facebook
- (NSString*)facebookAppId;

// twitter
- (NSString*)twitterConsumerKey;
- (NSString*)twitterSecret;
- (NSString*)twitterCallbackUrl;
- (NSNumber*)twitterUseXAuth;
- (NSString*)twitterUsername;

// view
- (NSNumber*)maxFavCount;

@end
