//
//  FlickitSharing.h
//  iPower
//
//  Created by edward liveikis on 5/21/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickitSharing : NSObject

+ (void)appShareGeneralInfoFromView:(UIView *)targetView;

+ (void)shareImage:(UIImage *)imageToShare;

+ (void)shareImage:(UIImage *)imageToShare inView:(UIView *)targetView;

@end
