//
//  SoundsController.h
//  iPower
//
//  Created by Daniel Moran on 6/1/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundsController : NSObject

+ (void) setBackgroundMusic:(BOOL) setOn;
+ (void) setGestureSound:(BOOL) setOn;
+ (void) setConsumeSound:(BOOL) setOn;
@end
