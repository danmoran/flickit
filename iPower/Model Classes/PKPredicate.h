//
//  PKPredicate.h
//  PKTest
//
//  Created by Petr Homola on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKPersistentObject.h"

@interface PKPredicate : NSObject {

}

- (BOOL)matchesObject:(id<PKPersistentObject>)object;
+ (PKPredicate*)predicateWithAttributeName:(NSString*)attName value:(id)_value;
+ (PKPredicate*)predicateWithAttributeNames:(NSArray*)attNames values:(id)_values;

@end
