//
//  MainSceneViewController.m
//  iPower
//
//  Created by Daniel Moran on 3/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//
///asdfasd

#import <AVFoundation/AVFoundation.h>
#import "MainSceneViewController.h"
#import "SubstanceObject.h"
#import "MenuViewController.h"
#import "MenuViewControlleriPad.h"
#import "LabView.h"
#import "SavedMixturesViewController.h"
#import "MixturesObject.h"
#import "PKObjectStore.h"
#import "PKAttributeValuePredicate.h"
#import "ContainerObject.h"
#import "Constants.h"
#import "PlaySounds.h"
//#import "AssignInertiaValues.h"
#import "LabPageViewControlleriPad.h"
//#import "SaveMixtureViewController.h"
#import "TutorialView.h"
#import "DeviceTypes.h"

#import "FlickitSharing.h"

@interface MainSceneViewController ()

@property (nonatomic, retain) LabView *labView;
@property (nonatomic, retain) SavedMixturesViewController *savedMixturesViewController;
@property (nonatomic, retain) ContainerObject *containerObject;
@property (nonatomic, retain) PlaySounds *playSounds;
@property (nonatomic, retain) MenuViewControlleriPad *menuiPad;
@property (nonatomic, retain) LabPageViewControlleriPad *labPageViewController;
//@property (nonatomic, retain) SaveMixtureViewController *saveViewController;
@property (nonatomic, retain) TutorialView *tutorialView;
@property (nonatomic, retain) UIPopoverController *popoverSettings;
@property (nonatomic, retain) UIPopoverController *popoverLabPage;
@property (nonatomic, retain) UIPopoverController *popoverSavedMixturesList;
@property (nonatomic, retain) NSDictionary *containerLevels;
@property (nonatomic, retain) UIView *backgroundView;

@property (nonatomic, assign) BOOL isConsumeModeOn;
@property (nonatomic, assign) BOOL shouldToogleImage;
@property (nonatomic, assign) BOOL shouldDisableTouchGestures;
@property (nonatomic, assign) BOOL isResetButtonOn;
@property (nonatomic, retain) NSTimer *consumeTimer;
@property (nonatomic, assign) CGPoint delta;
@property (nonatomic, assign) int startingAtCount;
@property (nonatomic, assign) CGFloat substanceCountMultiplier;
@property (nonatomic, assign) NSInteger circleHolder;

@end

@implementation MainSceneViewController {
    

}
@synthesize shareDialogView;
@synthesize actionBtnItem;
@synthesize menuBtnItem;
@synthesize navigationBar;
@synthesize popupMessageView;
@synthesize popupMessageLbl;



@synthesize substanceObject,labView,savedMixturesViewController, mixtureObject, containerObject, playSounds;
@synthesize labPageViewController = _labPageViewController;
//@synthesize saveViewController = _saveViewController;
@synthesize menuiPad;
@synthesize popoverSettings;
@synthesize popoverLabPage = _popoverLabPage;
@synthesize popoverSavedMixturesList = _popoverSavedMixturesList;
@synthesize containerLevels;
@synthesize backgroundView;
@synthesize gamePaused;
@synthesize menuBtn, pausePlayBtn, resetBtn, closeGamePausedBtn,exportBtn, consumeBtn;
@synthesize views;
@synthesize isConsumeModeOn = _isConsumeModeOn, shouldToogleImage, shouldDisableTouchGestures;
@synthesize consumeTimer;
@synthesize delta;
@synthesize startingAtCount;
@synthesize w = _w, h = _h;
@synthesize viewCenterX = _viewCenterX, viewCenterY = _viewCenterY;
//@synthesize shouldPlayBackgroundMusic = _shouldPlayBackgroundMusic;
@synthesize tutorialView = _tutorialView;
@synthesize substanceCountMultiplier = _substanceCountMultiplier, circleHolder =_circleHolder;
@synthesize isResetButtonOn = _isResetButtonOn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.substanceCountMultiplier = [DeviceTypes determineDeviceTypeMultiplier];
    self.circleHolder = [DeviceTypes circleHolderSizeHavingMultiplier:self.substanceCountMultiplier];
    
    
    self.w = self.view.frame.size.width;
    self.h = self.view.frame.size.height;
    
    self.playSounds = [PlaySounds playSounds];
    
    self.containerObject = [ContainerObject container];
    
    UIAccelerometer *accelerometer = [UIAccelerometer sharedAccelerometer];
    accelerometer.delegate = self;
    
    accelerometer.updateInterval = 1.0f/60.0f;
    
    gamePaused.hidden = YES;
    gamePaused.frame = CGRectMake(10, 100, 240, 380);
    [self.view addSubview:gamePaused];
    
    self.views  = [NSMutableArray array];
    
    [self loadLastSubstanceObjects];
    
    

    self.labView = [[[LabView alloc]initWithFrame:CGRectMake(0, 0, 320, 460)]autorelease];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        self.labView = [[[LabView alloc]initWithFrame:CGRectMake(0, 0, 500, 650)]autorelease];
    } 
    
    [self.labView setMainSceneViewController:self];
    [self.view addSubview:labView];
    self.labView.alpha = 0.0f;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.savedMixturesViewController = [[[SavedMixturesViewController alloc]initWithNibName:@"SavedMixturesViewController" bundle:nil]autorelease];
        
    } else {
        
        
        self.savedMixturesViewController = [[[SavedMixturesViewController alloc]initWithNibName:@"SavedMixturesViewControlleriPad" bundle:nil]autorelease];
        
    }
    
    [self.savedMixturesViewController setMainSceneViewController:self];
    
    self.substanceObject = [[SubstanceObject alloc]init];
    [self.substanceObject setMainSceneViewController:self];
    
    self.menuiPad = [MenuViewControlleriPad menu];
    [self.menuiPad setMainSceneViewController:self];
    

    [self setViewOrientationParametersForPopovers];

}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaultSettings = [NSUserDefaults standardUserDefaults];
    BOOL isAppFirstTimeOpened = [defaultSettings boolForKey:kIsAppFirstTimeOpened];   
    if (!isAppFirstTimeOpened) {
        
        [self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait];

        self.tutorialView = [TutorialView tutorial];
        UIImageView *tutorialImageView = [self.tutorialView tutorialViewhavingFrame:CGRectMake(0, 30, self.view.frame.size.width, self.view.frame.size.height -50)];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            BOOL isPortrait = UIDeviceOrientationIsPortrait(self.interfaceOrientation);
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                if (isPortrait)
                {
                    tutorialImageView = [self.tutorialView tutorialViewhavingFrame:CGRectMake(0, 0, self.w, self.h-100)];
                }
                else
                {

                    tutorialImageView = [self.tutorialView tutorialViewhavingFrame:CGRectMake(0, 0, self.w, self.h-100)];
                }
            }
            
        } else {
            
            [self presentMenuViewController];
        }
        
        [self.view addSubview:tutorialImageView];
        
//        self.popupMessageView.center = self.view.center;
        [self.view bringSubviewToFront:self.popupMessageView];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.w = self.view.frame.size.width;
    self.h = self.view.frame.size.height;
    
    self.consumeBtn.enabled = YES;
    self.exportBtn.enabled = YES;
    self.resetBtn.enabled = YES;
    
    [self.playSounds initializeSounds];
    
    UIImage *backgroundImage = [UIImage imageNamed:[[NSUserDefaults standardUserDefaults] objectForKey:kBackgroundKeyName]];
    if (backgroundImage){
     
        [self setBackgroundImageWithImage:backgroundImage];
    } else {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            
            UIImage *defaultBackgroundImage = [UIImage imageNamed:kBackground1];
            [self setBackgroundImageWithImage:defaultBackgroundImage];
            
        } else {
                   
            UIImage *defaultBackgroundImage = [UIImage imageNamed:kBackgroundPad1];
            [self setBackgroundImageWithImage:defaultBackgroundImage];
            
        }
    }
}

-(void) setBackgroundImageWithImage:(UIImage *)image {
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void) loadLastSubstanceObjects {
    
	[[PKObjectStore defaultObjectStore] fetchObjectsOfClass:[MixturesObject class] 
                           callback:^(NSArray* mixtures){
                               if ([mixtures count]>0) {

                                   self.mixtureObject = [mixtures lastObject];
                                   [self loadSelectedMixtureObject:self.mixtureObject];
                                   
                               }else {
//                                   TODO: I might not need the if statement below... no reason to differentiate between iPhone/iPad
                                   if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                                       
                                       [self loadSubstanceObjectsWithMaxDist:self.circleHolder minDist:0 startingCountAt:0 randomlocation:NO];
                                       
                                   } else {
                                       
                                       
                                       [self loadSubstanceObjectsWithMaxDist:self.circleHolder minDist:0 startingCountAt:0 randomlocation:NO];
                                       
                                   }
                                   
//                                   [self loadSubstanceObjectsWithMaxDist:42 minDist:0 startingCountAt:0 randomlocation:NO];;
                               } 
                        }]; 
}

-(void) loadSelectedMixtureObject:(MixturesObject *)mixture {
    
    for (UIView *view in self.views) {
        [view removeFromSuperview];
    }
    [self.views removeAllObjects];

    int i = 0;
    
    for (i = 0; [mixture.imageNames count] > i; i++) {
        
        NSValue *val = [mixture.centerPoints objectAtIndex:i];
        CGPoint xPoint = val.CGPointValue;
        /// converts MixtureObject format to self.views format.
        self.substanceObject = [[[SubstanceObject alloc]initWithFrame:CGRectZero]autorelease];
        UIImage *image = [UIImage imageNamed:[mixture.imageNames objectAtIndex:i]];
        self.substanceObject.image = image;
        self.substanceObject.imageName = [mixture.imageNames objectAtIndex:i];
        self.substanceObject.substanceType = [mixture.types objectAtIndex:i];
//        self.substanceObject.inertia = [[mixture.inertiaValues objectAtIndex:i]floatValue];
        [self.substanceObject setCenter:xPoint];
        
        [self.views addObject:self.substanceObject];
        [self.view addSubview:self.substanceObject];
    }
}

-(void) loadSubstanceObjectsWithMaxDist:(int) dist1 minDist:(int)dist2 startingCountAt:(int) startingCount randomlocation:(BOOL) useRandomPlacement {
        
    CGFloat w = self.w;
    CGFloat h = self.h;
    CGFloat xMinPosibleVal = w*.30;
    CGFloat xMaxPosibleVal = w - (w*.30);
    CGFloat yMaxPosibleVal = h;
    CGFloat xCenterPoint = w/2;
    CGFloat yCenterPoint = h/2;
    CGFloat x = xCenterPoint - 100;
    CGFloat y  = yCenterPoint - 100;

    
    int numberOfSubstanceObjects = kNumberOfSubtanceObjects * self.substanceCountMultiplier;
    

    
    for (int i = startingCount; i < numberOfSubstanceObjects; i++) {
        
        self.startingAtCount = i;
        
        int randomImageNumber = arc4random()%8;
        if (useRandomPlacement) randomImageNumber = (arc4random()%(12-7))+7; //range from 7 to 12
     
        randomImageNumber++;
        NSMutableString *imageName = [NSMutableString stringWithString:@"substanceWhite"];
        [imageName appendString:[NSString stringWithFormat:@"%i", randomImageNumber]];

        UIImage *image = [UIImage imageNamed:imageName];
        
        self.substanceObject = [[[SubstanceObject alloc]initWithFrame:CGRectZero]autorelease];
        
        CGPoint centerView = [self.substanceObject viewsCenterPointWith:x andYValue:y];        
        CGPoint screenCenterPoint = CGPointMake(xCenterPoint, yCenterPoint);
        CGFloat dx = screenCenterPoint.x - centerView.x;
        CGFloat dy = screenCenterPoint.y - centerView.y;
        CGFloat distance = sqrtf(dx*dx + dy*dy);

        if (distance < dist1 && distance > dist2) {
            
            self.substanceObject.image = image;
            self.substanceObject.imageName = imageName;
            self.substanceObject.substanceType = kContainer1;
//            self.substanceObject.inertia = kMinInertiaValue;
            [self.substanceObject setCenter:centerView];
            [self.view addSubview:self.substanceObject];        
            [self.views addObject: self.substanceObject];
        }
        else {
            i--;
            
            if (y>yMaxPosibleVal) {
                
                break;
            }
        }
        
        if (useRandomPlacement) {
            int xRandomLocation = arc4random()%30;
            
            x = x + kObjectSize + xRandomLocation;
            
            if (x > xMaxPosibleVal) {
                x = xMinPosibleVal;
                y = y + kObjectSize + xRandomLocation;
            }
            
        }else {
            x = x + kObjectSize -4;
            
            if (x > xMaxPosibleVal) {
                x = xMinPosibleVal;
                y = y + kObjectSize -4;
            }
        }
    }
}
// test comment... bitbucket issue.
- (IBAction)redraw:(id)sender {
    
    for (UIView *view in self.views) {
        [view removeFromSuperview];
    }
    [self.views removeAllObjects];
    
//    TODO: double check if I need the if statement below...
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self loadSubstanceObjectsWithMaxDist:self.circleHolder minDist:0 startingCountAt:0 randomlocation:NO];
        
    } else {
        
        [self loadSubstanceObjectsWithMaxDist:self.circleHolder minDist:0 startingCountAt:0 randomlocation:NO];
        
    }
    
    //    [self loadSubstanceObjectsWithMaxDist:42 minDist:0 startingCountAt:0 randomlocation:NO];
    
    NSLog(@"startingCount = %i", self.startingAtCount);
    NSLog(@"substance count = %f", (kNumberOfSubtanceObjects * self.substanceCountMultiplier));
    
    while (self.startingAtCount < kNumberOfSubtanceObjects * self.substanceCountMultiplier -1) {
        
        [self loadSubstanceObjectsWithMaxDist:80 minDist:40 startingCountAt:self.startingAtCount randomlocation:YES];
    }
    self.isConsumeModeOn = NO;
    UIImage *image = [UIImage imageNamed:kConsumeBtnOn];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        image = [UIImage imageNamed:kConsumePadBtnOn];
    }
    [self.consumeBtn setImage:image forState:UIControlStateNormal];
    
    
    //NOTE: code below creates the explotion effect.
    [playSounds playExplosionSound];
    
    CGFloat w = self.w;
    CGFloat h = self.h;
    CGFloat xCenterPoint = w/2;
    CGFloat yCenterPoint = h/2;
    
    NSMutableArray *smokeImages  = [NSMutableArray array];
    int i = 0;
    for (i = 1; 18 > i; i++) {
        NSMutableString *imageName = [NSMutableString stringWithString:@"smokeSequence"];
        [imageName appendString:[NSString stringWithFormat:@"%i.png", i]];
        UIImage *image = [UIImage imageNamed:imageName];
        [smokeImages addObject:image];
    }
    
    UIImageView *imageViewAnimation = [[[UIImageView alloc]initWithFrame:CGRectMake(xCenterPoint, yCenterPoint, 200, 200)]autorelease];
    imageViewAnimation.center = CGPointMake(xCenterPoint, yCenterPoint);
    imageViewAnimation.backgroundColor = [UIColor clearColor];
    imageViewAnimation.animationImages = smokeImages;
    imageViewAnimation.animationDuration = 0.75;
    imageViewAnimation.animationRepeatCount = 1;
    [imageViewAnimation startAnimating];
    
    [self.view addSubview:imageViewAnimation];
    //    [playSounds stopExplosionSound];

    UIImage *image2 = [UIImage imageNamed:@"redraw.png"];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        image2 = [UIImage imageNamed:@"redrawPad.png"];
        
    }
    
    
    [self.resetBtn setImage:image2 forState:UIControlStateNormal];
    self.isResetButtonOn = NO;
}

- (IBAction)presentLabView:(id)sender {
    
    if ([self.resetBtn isEnabled]) {
        self.exportBtn.enabled = NO;
        self.consumeBtn.enabled = NO;
        self.resetBtn.enabled = NO;
        self.shouldDisableTouchGestures = YES;
        
        [self.view bringSubviewToFront:self.labView];
        
        [UIView animateWithDuration: 0.4 animations: ^{
            self.labView.alpha = 1.0f;
        }];
        
        self.containerLevels = [NSDictionary dictionary];
        self.containerLevels = [self.containerObject updateContainerlevelsUsingMixtureObject:self.views];
        [self.labView passValuesToSlidersFromDictionary:self.containerLevels];
//        [self.playSounds stopBackgroundSound];
        
    } else {
        
        self.exportBtn.enabled = YES;
        self.consumeBtn.enabled = YES;
        self.resetBtn.enabled = YES; 
        self.shouldDisableTouchGestures = NO;
    
        if (self.isResetButtonOn) {
            UIImage *image2 = [UIImage imageNamed:@"redraw.png"];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                image2 = [UIImage imageNamed:@"redrawPad.png"];
                
            }
            
            
            [self.resetBtn setImage:image2 forState:UIControlStateNormal];
            
            self.isResetButtonOn = NO;
        }

//        if (self.shouldPlayBackgroundMusic) {
//            [self.playSounds playBackgroundSound];
//        }
//        self.shouldPlayBackgroundMusic = YES;
    }
}
- (IBAction)presentMenuViewControlleriPad:(id)sender {
    
    self.popoverSettings = [[[UIPopoverController alloc]initWithContentViewController:menuiPad]autorelease];
    self.popoverSettings.popoverContentSize=CGSizeMake(320,700);
    self.popoverSettings.delegate = self;
    
    CGRect selectedRect = CGRectMake(30,35,1,1);
    [self.popoverSettings presentPopoverFromRect:selectedRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES]; 
    

}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    
    // NOTE: dismiss popover for iPad menu
    [self.playSounds initializeSounds];
}

-(void) presentMenuViewController {
    
    MenuViewController *menu = [[[MenuViewController alloc]init]autorelease];
    [self presentModalViewController:menu animated:YES];
    
    self.shouldDisableTouchGestures = NO;
//    [self.playSounds stopBackgroundSound];
}

-(void) presentSavedMixturesViewController {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self presentModalViewController:self.savedMixturesViewController animated:YES];
        
        
    } else {
        
        self.popoverSavedMixturesList = [[[UIPopoverController alloc]initWithContentViewController:self.savedMixturesViewController]autorelease];
        self.popoverSavedMixturesList.popoverContentSize=CGSizeMake(420,550);
        self.popoverSavedMixturesList.delegate = self;
        
        CGRect selectedRect = CGRectMake(self.viewCenterX, self.viewCenterY,1,1);
        [self.popoverSavedMixturesList presentPopoverFromRect:selectedRect inView:self.view permittedArrowDirections:0 animated:YES]; 
        
        self.savedMixturesViewController.selectionHandler = ^{
            
            [self.popoverSavedMixturesList dismissPopoverAnimated:YES];          
        };
    }
}


-(void) saveMixtureInDBWithName:(NSString *)name {
    
    MixturesObject *mixture = [[[MixturesObject alloc]init]autorelease];
    
    mixture.name = name;
    mixture.dateSaved = [NSDate date];
    
    ///convert self.views to mixtureObject format before saving the mixture in DB.
    NSMutableArray *tempPoints = [NSMutableArray array];
    NSMutableArray *tempImagesNames = [NSMutableArray array];
    NSMutableArray *tempTypes = [NSMutableArray array];  
//    NSMutableArray *tempInertia = [NSMutableArray array];  
    
    for (SubstanceObject *substance in self.views) {       
        
        [tempPoints addObject:[NSValue valueWithCGPoint:substance.center]];
        [tempImagesNames addObject:substance.imageName];
        [tempTypes addObject:substance.substanceType];
//        [tempInertia addObject:[NSNumber numberWithFloat:substance.inertia]];
    }
    
    mixture.centerPoints = tempPoints;
    mixture.imageNames = tempImagesNames;
    mixture.types = tempTypes;

    
    [[PKObjectStore defaultObjectStore] storeObject: mixture callback: ^(BOOL success) {}];
    
}

- (void) pulseConsumeButton
{
    
    UIImage * consumeModeOFF = [UIImage imageNamed:kConsumeBtn];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        consumeModeOFF = [UIImage imageNamed:kConsumePadBtn];
    }
    
    UIImage * consumeModeON = [UIImage imageNamed:kConsumeBtnOn];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        consumeModeON = [UIImage imageNamed:kConsumePadBtnOn];
    }
    
    if (self.isConsumeModeOn)
    {
        if (shouldToogleImage) {
            
            [self.consumeBtn setImage:consumeModeON forState:UIControlStateNormal];
            shouldToogleImage = NO;
        }
        else {
            
            [self.consumeBtn setImage:consumeModeOFF forState:UIControlStateNormal];
            shouldToogleImage = YES;
        }
    } else {

        [self.consumeTimer  invalidate];
    }
}

- (IBAction)consume:(id)sender {

    if (self.isConsumeModeOn) {
        
        self.isConsumeModeOn = NO;
        UIImage * consumeModeOFF = [UIImage imageNamed:kConsumeBtnOn];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            consumeModeOFF = [UIImage imageNamed:kConsumePadBtnOn];
        }
        
        [self.consumeBtn setImage:consumeModeOFF forState:UIControlStateNormal];
        
    }
    else {
        

        self.isConsumeModeOn = YES;
        UIImage * consumeModeON = [UIImage imageNamed:kConsumeBtnOn];

        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            consumeModeON = [UIImage imageNamed:kConsumePadBtnOn];
        }
        
        [self.consumeBtn setImage:consumeModeON forState:UIControlStateNormal];
        self.consumeTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self 
                                                                 selector:@selector(pulseConsumeButton) 
                                                                 userInfo:nil 
                                                                  repeats:YES];
    }
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
//    : sort self.views by inertial value
//    : instead of processing the entire self.views determine an amount at a time so the flow of items sliding feel more natural and focus in objects with the least inertial after sorting the self.views array.....
    
//    
//    BOOL moveObject = NO;
//    
//    delta.x = acceleration.x *10;
//    delta.y = acceleration.y *10;    
//    
//    CGFloat xAngle = ABS(acceleration.x);
//    CGFloat yAngle = ABS(acceleration.y);
//    
//    CGFloat angle = xAngle > yAngle ? xAngle : yAngle;
//    
//    
//    int movedObjectsCount = 0;
//    
//    if (angle >40) {
//        
//        [AssignInertiaValues calculateSubstanceDistanceAndAssignInertiaLevel:self.views];
//    }
//    
//    if (angle > 0.50) {
//        int i = 0;
//        for (i = 0;[self.views count]>i; i++){
//            
//            moveObject = NO;
//            
//            self.substanceObject = [self.views objectAtIndex:i];
//
//            
//            if (self.substanceObject.inertia >= 9.0f){
//                moveObject = YES;
//            }else if (angle> 0.47 && angle < 0.50 && (self.substanceObject.inertia > 5.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.53 && angle < 0.55  && (self.substanceObject.inertia > 4.5f)) {
//                moveObject = YES;
//            }else if (angle> 0.55  && (self.substanceObject.inertia > 3.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.60  && (self.substanceObject.inertia > 2.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.65  && (self.substanceObject.inertia >= 0.0f)) {
//                moveObject = YES;
//            }
//
////            if (self.substanceObject.inertia >= 9.0f){
////                moveObject = YES;
////            }else if (angle> 0.30 && angle < 0.33 && (self.substanceObject.inertia > 8.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.33 && angle < 0.36 && (self.substanceObject.inertia > 7.5f)) {
////                moveObject = YES;
////            }else if (angle> 0.36 && angle < 0.39 && (self.substanceObject.inertia > 7.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.39 && angle < 0.41 && (self.substanceObject.inertia > 6.5f)) {
////                moveObject = YES;
////            }else if (angle> 0.41 && angle < 0.44 && (self.substanceObject.inertia > 6.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.44 && angle < 0.47 && (self.substanceObject.inertia > 5.5f)) {
////                moveObject = YES;
////            }else if (angle> 0.47 && angle < 0.50 && (self.substanceObject.inertia > 5.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.53 && angle < 0.55  && (self.substanceObject.inertia > 4.5f)) {
////                moveObject = YES;
////            }else if (angle> 0.55  && (self.substanceObject.inertia > 3.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.60  && (self.substanceObject.inertia > 2.0f)) {
////                moveObject = YES;
////            }else if (angle> 0.65  && (self.substanceObject.inertia >= 0.0f)) {
////                moveObject = YES;
////            }
//            
//            if (moveObject) {
//                
//                CGFloat xValue = self.substanceObject.center.x;
//                CGFloat yValue = self.substanceObject.center.y;
//                
//                if (xValue >0 && xValue < 350 && yValue >0 && yValue < 500) {
//                    self.substanceObject.center = CGPointMake(self.substanceObject.center.x + delta.x, self.substanceObject.center.y - delta.y);
//                    
//                    movedObjectsCount++;
//                    if (movedObjectsCount>30) {
//                        i = 290;
//                    }
//                }
//                
//                
//                
//                
//                
////                CGRect movingObject = substanceObject.frame;
////                
////                for (i = 0; [self.views count]>i; i++) {
////                    
////                    SubstanceObject *substanceObjectStanding = [self.views objectAtIndex:i];
////                    
////                    CGRect standingObject = substanceObjectStanding.frame;
////                    
////                    if (CGRectIntersectsRect(movingObject, standingObject)) {
////                        
////                        substanceObject.inertia = substanceObjectStanding.inertia;
////                    }
////                }
//            }
//        }
//    }
}


#pragma mark Touch handling


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (!self.shouldDisableTouchGestures) {
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocationPoint = [touch locationInView:self.view]; 

    
    CGRect touchSize = CGRectMake(touchLocationPoint.x - kTouchSize/2, touchLocationPoint.y - kTouchSize/2, kTouchSize, kTouchSize);
    CGRect viewsWithinTouchSizeBuffer = CGRectMake(touchLocationPoint.x - kTOuchSizeBuffer/2, touchLocationPoint.y - kTOuchSizeBuffer/2, kTOuchSizeBuffer, kTOuchSizeBuffer);
    
    int i = 0;
    /// NOTE: isConsumeModeOn (BOOL) determines whether the app is on consume mode.
    if (self.isConsumeModeOn) {
        
        [self.playSounds snortSoundVolumeUp];
        
        for (i = 0;[self.views count]>i; i++){
            
            self.substanceObject = [self.views objectAtIndex:i];
            CGRect frame = self.substanceObject.frame;
            
            if (CGRectIntersectsRect(touchSize, frame)) {
                self.substanceObject.hidden = YES;
                self.substanceObject.substanceType = kContainerx;
            }
        }
    }    
    else {
        
        [self.playSounds turnDraggingSoundUp];
    
        int i = 0;
        NSMutableArray *viewsInTouchBuffer = [[[NSMutableArray alloc]init]autorelease];
        
        for (i = 0;[self.views count]>i; i++){
            
            self.substanceObject = [self.views objectAtIndex:i];
            CGRect frame = self.substanceObject.frame;
            /// NOTE: looks for substance (views) inside the touch rectangle
            if (CGRectIntersectsRect(viewsWithinTouchSizeBuffer, frame)) {
                
                [viewsInTouchBuffer addObject:[NSNumber numberWithInt:i]];

                //for iPhone use, prevents the app from lagging.
                if ([viewsInTouchBuffer count]>30 && [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) break;
                
                /// NOTE:potential substances that are modifed, we reset its inertia value to zero.
//                self.substanceObject.inertia = 0.0f;  TODO: remove all inertial related code.
            }
        }
        
        CGFloat angle = 0.0;
        
        for (i = 0;[self.views count]>i; i++){
            
            self.substanceObject = [self.views objectAtIndex:i];
            CGRect frame = self.substanceObject.frame;
            
            if (CGRectIntersectsRect(touchSize, frame)) {
                CGPoint objectPoint = frame.origin;
                CGFloat xDiff = objectPoint.x - (touchLocationPoint.x - kObjectSize/2);
                CGFloat yDiff = objectPoint.y - (touchLocationPoint.y - kObjectSize/2);
                CGFloat tan = sqrt (powf(xDiff, 2.0)+powf(yDiff, 2.0));
                
                angle = (asinf(yDiff/tan))* 180/M_PI;
                
                if (angle < 75.0 && angle > 45.0){
                    angle = angle + 15.0;
                }else if (angle < 45.0 && angle >20.0){
                    angle = angle -15.0;
                }else if (angle < -20.0 && angle > -45.0) {
                    angle = angle +  15.0;
                }else if (angle < -45.0 && angle > -75.0) {
                    angle = angle -15.0;
                }
                
                /// NOTE: this can't be smaller than touchSize width or height
                CGFloat desiredDistance  = kTouchSize * 1.3  - tan; 
                
                CGFloat newSine =  (sinf((angle)*(M_PI/180)))*desiredDistance;
                CGFloat newCosine =  (cosf((angle)*(M_PI/180)))*desiredDistance;
                if (xDiff < 0 ) {
                    newCosine = newCosine * -1;
                }

                frame = CGRectMake(objectPoint.x + newCosine, objectPoint.y + newSine, frame.size.width, frame.size.height);
                
                //NOTE: avoid substances from going off screen limits
                CGFloat xVal = frame.origin.x;
                CGFloat yVal = frame.origin.y;
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                    
                    if (xVal>30 && xVal <290 && yVal > 50 && yVal <360){
                        
                    }else continue;
                    
                } else {
                    
                    if (xVal>50 && xVal <700 && yVal > 60 && yVal <600){
                        
                    }else continue;
                }
                
                self.substanceObject.frame = frame;  
                [self.view bringSubviewToFront:self.substanceObject];

                for (NSNumber *index in viewsInTouchBuffer) {  
                    
                    int i = index.integerValue;
                    
                    UIImageView *viewToRelocate = [self.views objectAtIndex:i]; 
                    
                    CGPoint touchLocationPoint = self.substanceObject.frame.origin;
                    CGRect touchSize = self.substanceObject.frame;
                    CGRect frame = viewToRelocate.frame;
                    
                    if (CGRectIntersectsRect(touchSize, frame)){
                        
                        CGPoint objectPoint = frame.origin;
                        
                        CGFloat xDiff = objectPoint.x - (touchLocationPoint.x);
                        CGFloat yDiff = objectPoint.y - (touchLocationPoint.y);
                        CGFloat tan = sqrt (powf(xDiff, 2.0) + powf(yDiff, 2.0));
                        
                        if ((tan< kObjectSize * 0.3)&& (tan !=0)) {
                            
                            CGFloat desiredDistance  = kObjectSize - tan;
                            
                            CGFloat newSine =  (sinf((angle)*(M_PI/180)))*desiredDistance; //use same angle as original vertex
                            CGFloat newCosine =  (cosf((angle)*(M_PI/180)))*desiredDistance;
                            if (xDiff < 0 ) {
                                newCosine = newCosine * -1;
                            }
                            
                            frame = CGRectMake(objectPoint.x + newCosine, objectPoint.y + newSine, frame.size.width, frame.size.height);
                            viewToRelocate.frame = frame; 
                            
                            [self.view bringSubviewToFront:viewToRelocate];
                        }
                    }   
                }         
            }
        }
    }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
    [self.playSounds turnDraggingSoundDown];
    
    int i = 0;
    int hiddenObjectsCount = 0;
    for (i = 0;[self.views count]>i; i++){
        
        self.substanceObject = [self.views objectAtIndex:i];
        
        if (substanceObject.hidden) {
            hiddenObjectsCount++;
        }
//        CGRect frame = self.substanceObject.frame;
//        
//        if (CGRectIntersectsRect(touchSize, frame)) {
//            self.substanceObject.hidden = YES;
//            self.substanceObject.substanceType = kContainerx;
//        }
    }

    
    if (self.isConsumeModeOn) {
        
//        [self.playSounds playSnortEndingSound];
        [self.playSounds snortSoundVolumeDown];
        
        int randomImageNumber = arc4random()%5; //TODO: final product replace 5 with 20
        
        if (randomImageNumber == 1) {
            
            [UIView animateWithDuration: 0.4 animations: ^{
                self.popupMessageView.alpha = 1.0f;
            }];
            [self.view bringSubviewToFront:self.popupMessageView];
            self.popupMessageLbl.text = @"You consumed less than an avearage amount don't be affraid these are virtual substances with 0 side effects!!!";
        }
        
        
     
        
//        if (randomImageNumber == 7) {
//            
//            UIAlertView *consumeMessage = [[UIAlertView alloc]initWithTitle:@"You are a wimp!!" message:@"You consumed less than an avearage amount don't be affraid these are virtual substances with 0 side effects!!!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//            
//            [consumeMessage show];
//        }
    }
    
    NSLog(@"hiddenObjectsCount %i", hiddenObjectsCount);
    if (hiddenObjectsCount ==[self.views count]) {
        NSLog(@"hiddenCount is equal to self.View");
        [self consume:nil];
        UIImage *image = [UIImage imageNamed:@"redrawON.png"];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            image = [UIImage imageNamed:@"redrawONPad.png"];
            
        }
        [self.resetBtn setImage:image forState:UIControlStateNormal];
        self.isResetButtonOn = YES;

    }
    
}
- (IBAction)hidePopupMessageView:(id)sender {
    
//    self.popupMessageView.al
    
    [UIView animateWithDuration: 0.4 animations: ^{
        self.popupMessageView.alpha = 0.0f;
    }];
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

#pragma mark - sharing
- (IBAction)shareButtonClicked:(id)sender {
    
    //    [FlickitSharing shareImage:[self imageByRenderingMainSceneView] inView:self.view];

    
    
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [FlickitSharing shareImage:[self imageByRenderingMainSceneView] inView:self.view];
                
    } else {
        [FlickitSharing shareImage:[self imageByRenderingMainSceneView]
                            inView:self.view];//self.shareDialogView];  //TODO: remove the shareDialogView
        
        
        

    }
}

-(UIImage *) imageByRenderingMainSceneView {
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        CGFloat height = self.view.bounds.size.height -90;
        CGSize imageSize = CGSizeMake(self.view.bounds.size.width, height);
        
        UIGraphicsBeginImageContext(imageSize);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return resultingImage;
        
    } else {
        
        [self.navigationBar setHidden:YES];
        
        CGFloat height = self.view.bounds.size.height -170;
        CGSize imageSize = CGSizeMake(self.view.bounds.size.width, height);
        
        UIGraphicsBeginImageContext(imageSize);
        
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
         
        [self.navigationBar setHidden:NO];
        return resultingImage;
    }
    
}

#pragma mark -

-(void) updateSubstanceTypesOnScreenWithValuesIn:(NSDictionary *) containerValues {

    self.containerLevels = containerValues;    
    self.views = [self.containerObject udpateMixtureSubstanceTypesUsing:containerValues forSubstances:self.views];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//
//        return (interfaceOrientation == UIInterfaceOrientationPortrait);
//        
//    } else {
//        
//        return YES;
//    }
//    TODO: remove all code related to autorotate.
    
    return NO;
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration 
{
    
    [self setViewOrientationParametersForPopovers];

}
-(void) setViewOrientationParametersForPopovers {
    
    self.w = self.view.frame.size.width;
    self.h = self.view.frame.size.height;
    
//    UIDeviceOrientation orientation = [[UIDevice currentDevice]orientation];

    BOOL isPortrait = UIDeviceOrientationIsPortrait(self.interfaceOrientation);
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        if (isPortrait)
        {
            self.w = self.view.frame.size.width;
            self.h = self.view.frame.size.height;
            self.viewCenterX = self.view.center.x;
            self.viewCenterY = self.view.center.y;
        }
        else
        {
            self.w = self.view.frame.size.height;
            self.h = self.view.frame.size.width;
            self.viewCenterX = self.view.center.y;
            self.viewCenterY = self.view.center.x;
        }
        CGPoint center = CGPointMake(self.viewCenterX, self.viewCenterY);
        self.labView.center = center;
    }
}

- (void)viewDidUnload
{
    [self setPausePlayBtn:nil];
    [self setMenuBtn:nil];
    [self setGamePaused:nil];
    [self setCloseGamePausedBtn:nil];
    [self setResetBtn:nil];
    [self setExportBtn:nil];
    [self setConsumeBtn:nil];
    [self setShareDialogView:nil];
    [self setActionBtnItem:nil];
    [self setMenuBtnItem:nil];
    [self setNavigationBar:nil];
    [self setPopupMessageView:nil];
    [self setPopupMessageLbl:nil];
    [super viewDidUnload];
}

- (void)dealloc {

    [containerObject release];
    [pausePlayBtn release];
    [menuBtn release];
    [gamePaused release];
    [closeGamePausedBtn release];
    [resetBtn release];
    [exportBtn release];
    [consumeBtn release];
    [shareDialogView release];
    [actionBtnItem release];
    [menuBtnItem release];
    [navigationBar release];
    [popupMessageView release];
    [popupMessageLbl release];
    [super dealloc];
}
@end



