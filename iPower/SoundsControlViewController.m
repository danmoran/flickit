//
//  SoundsControlViewController.m
//  iPower
//
//  Created by Daniel Moran on 5/15/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "SoundsControlViewController.h"
#import "SoundsController.h"
#import "Constants.h"
@interface SoundsControlViewController ()

@end

@implementation SoundsControlViewController
@synthesize backgroundSwitch;
@synthesize swipesSwitch;
@synthesize consumeSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSUserDefaults *isOn = [NSUserDefaults standardUserDefaults];
    self.backgroundSwitch.on = [isOn boolForKey:kPlayBackgroundMusic];
    self.swipesSwitch.on = [isOn boolForKey:kPlayDraggingSound];
    self.consumeSwitch.on = [isOn boolForKey:kPlayConsumeSound];
    
}
- (IBAction)presentMenuViewController:(id)sender {
    
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction)backgroundValueChanged:(id)sender {
    
    BOOL isOn = [self.backgroundSwitch isOn];
    
    [SoundsController setBackgroundMusic: isOn];
    
//    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
//    
//    if (playSound) {
//        
//        [shouldPlaySound setBool:YES forKey:kPlayBackgroundMusic];
//    } else {
//        [shouldPlaySound setBool:NO forKey:kPlayBackgroundMusic];
//    }
}
- (IBAction)swipeValueChanged:(id)sender {
    
    BOOL isOn = [self.swipesSwitch isOn];
    
    [SoundsController setGestureSound:isOn];
    
//    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
//    
//    if (playSound) {
//        
//        [shouldPlaySound setBool:YES forKey:kPlayDraggingSound];
//    } else {
//        [shouldPlaySound setBool:NO forKey:kPlayDraggingSound];
//    }
}
- (IBAction)consumeModeValueChanged:(id)sender {
    
    BOOL isOn = [self.consumeSwitch isOn];
    
    [SoundsController setConsumeSound:isOn];
    
//    NSUserDefaults *shouldPlaySound = [NSUserDefaults standardUserDefaults];
//    
//    if (playSound) {
//        
//        [shouldPlaySound setBool:YES forKey:kPlayConsumeSound];
//    } else {
//        [shouldPlaySound setBool:NO forKey:kPlayConsumeSound];
//    }
}


- (void)viewDidUnload
{
    [self setBackgroundSwitch:nil];
    [self setSwipesSwitch:nil];
    [self setConsumeSwitch:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [backgroundSwitch release];
    [swipesSwitch release];
    [consumeSwitch release];
    [super dealloc];
}
@end
