//
//  SavedMixturesViewController.m
//  iPower
//
//  Created by Daniel Moran on 4/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//
#import "QuartzCore/QuartzCore.h"
#import "SavedMixturesViewController.h"
#import "MainSceneViewController.h"
#import "MixturesObject.h"
#import "PKObjectStore.h"
#import "PKAttributeValuePredicate.h"
#import "SubstanceObject.h"

@interface SavedMixturesViewController ()

@property (nonatomic, retain) MainSceneViewController *mainScene;
@property (nonatomic, retain) NSMutableArray *mixtureNames;
@property (nonatomic, retain) NSMutableArray *mixtureObjectCollection;
@property (nonatomic, assign) BOOL isEditModeOn;

@end

@implementation SavedMixturesViewController

@synthesize mainScene;
@synthesize mixtureNames, mixtureObjectCollection;
@synthesize dataTableView;
@synthesize editBtn;
@synthesize backBtn;
@synthesize backgroundImage;
@synthesize isEditModeOn;
@synthesize selectionHandler;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
    
    self.mainScene = _mainSceneViewController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataTableView.delegate = self;
    self.dataTableView.dataSource = self;
    self.dataTableView.layer.cornerRadius = 10;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self populateTableView];
}

- (IBAction)backToLabPage:(id)sender {
    [self setEditing:NO animated:NO];
    isEditModeOn = NO;
    [self dismissModalViewControllerAnimated:YES];
    [self.mainScene presentLabView:nil];
    
}


-(void) populateTableView {
    
    __block SavedMixturesViewController* me = self;
    
	[[PKObjectStore defaultObjectStore] fetchObjectsOfClass:[MixturesObject class] 
                               callback:^(NSArray* mixtures){
                                   if ([mixtures count]>0) {

                                       me.mixtureNames = [NSMutableArray array];
                                       me.mixtureObjectCollection = [NSMutableArray array];
                                       
                                       for (MixturesObject *mix in mixtures) {
                                    
                                           [me.mixtureNames addObject:mix.name];
                                           [me.mixtureObjectCollection addObject:mix];
                                       }
                                       
                                       [me.dataTableView reloadData];
                                   } 
                               }]; 
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.mixtureNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil) {
        
        cell = [[[UITableViewCell alloc]init]autorelease];
    }
    cell.textLabel.text = [self.mixtureNames objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self dismissModalViewControllerAnimated:YES];
    [self.mainScene loadSelectedMixtureObject:[self.mixtureObjectCollection objectAtIndex:indexPath.row]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        selectionHandler();
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
     return YES;
 }

- (IBAction)startEditMode:(id)sender {
 
    if (isEditModeOn) {
        [self setEditing:NO animated:YES];
        isEditModeOn = NO;
    }else {
        [self setEditing:YES animated:YES];
        isEditModeOn = YES;
    }
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing: editing animated: animated];
    [self.dataTableView setEditing:editing animated:animated];
    
}
 
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         
         [self.mixtureNames removeObjectAtIndex:indexPath.row];
         
         MixturesObject *mixToDelete = [self.mixtureObjectCollection objectAtIndex:indexPath.row];
         
         [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
         
          [[PKObjectStore defaultObjectStore] deleteObject: mixToDelete callback: ^(BOOL success) {
              
              [self.mixtureObjectCollection removeObjectAtIndex:indexPath.row];
          }];
     }

    [self.dataTableView reloadData];
}
 
- (IBAction)dismissSavedMixturesViewControlleriPad:(id)sender {

    selectionHandler();
}



#pragma mark - Table view delegate

- (void)viewDidUnload
{
    [self setEditBtn:nil];
    [self setBackBtn:nil];
    [self setDataTableView:nil];
    [self setBackgroundImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [editBtn release];
    [backBtn release];
    [dataTableView release];
    [backgroundImage release];
    [selectionHandler release];
    
    [super dealloc];
}
@end
