//
//  MixturesObject.m
//  iPower
//
//  Created by Daniel Moran on 4/10/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "MixturesObject.h"

@implementation MixturesObject

@synthesize centerPoints;
@synthesize types;
@synthesize name;
@synthesize imageNames;
//@synthesize inertiaValues;
@synthesize dateSaved;

@end
