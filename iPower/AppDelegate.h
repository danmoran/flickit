//
//  AppDelegate.h
//  iPower
//
//  Created by Daniel Moran on 2/11/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainSceneViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) MainSceneViewController *mainScene;
//@property (strong, nonatomic) ViewController *viewController;

@end
