//
//  SubstancesViewController.m
//  iPower
//
//  Created by Daniel Moran on 2/22/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "SubstancesViewController.h"

@implementation SubstancesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    UIImage *substance1 = [UIImage imageNamed:@"powderRed.png"];
    UIImage *substance2 = [UIImage imageNamed:@"powderIvory.png"];
    UIImage *substance3 = [UIImage imageNamed:@"powderGreen.png"];
    UIImage *substance4 = [UIImage imageNamed:@"powderOrange.png"];
    
    substanceImages = [[NSArray arrayWithObjects:substance1, substance2, substance3, substance4, nil]retain];
    
    NSString *substance1Description = NSLocalizedString(@"substance 1", nil);
    NSString *substance2Description = NSLocalizedString(@"substance 2", nil);
    NSString *substance3Description = NSLocalizedString(@"substance 3", nil);
    NSString *substance4Description = NSLocalizedString(@"substance 4", nil);
    
    substanceDescription = [[NSArray arrayWithObjects:substance1Description, substance2Description, substance3Description, substance4Description, nil]retain];
    currentIndex = 0;
    
    substanceLbl.text = NSLocalizedString(@"substance 1", nil);
    [previousBtn setTitle: NSLocalizedString(@"previous", nil) forState: UIControlStateNormal];
    [selectBtn setTitle: NSLocalizedString(@"select", nil) forState: UIControlStateNormal];
    [nextBtn setTitle: NSLocalizedString(@"next", nil) forState: UIControlStateNormal];
    
    
}
- (IBAction)selectSubstance:(id)sender {
    
    NSString *imageName = [substanceImages objectAtIndex:currentIndex];
    
    NSUserDefaults *substanceImage = [NSUserDefaults standardUserDefaults];
    [substanceImage setObject:imageName forKey:@"substanceImage"];
    
    [self dismissModalViewControllerAnimated:NO];
}

- (IBAction)presentPreviousSubstanceImage:(id)sender {
    
    currentIndex --;
    if (currentIndex < 0) {
        currentIndex = [substanceImages count]-1;
    }
    imageView.image = [substanceImages objectAtIndex:currentIndex];
    
    
    substanceLbl.text = NSLocalizedString(@"GREETINGS", nil);
    
    substanceLbl.text = [substanceDescription objectAtIndex:currentIndex];
}

- (IBAction)presentNextSubstanceImage:(id)sender {

    currentIndex ++;
    if (currentIndex > [substanceImages count]-1) {
        currentIndex = 0;
    }
    imageView.image = [substanceImages objectAtIndex:currentIndex];
    substanceLbl.text = [substanceDescription objectAtIndex:currentIndex];
}

- (IBAction)selectedPowder:(id)sender {
    
    NSString *imageName = [substanceImages objectAtIndex:currentIndex];
    
    NSUserDefaults *substanceImage = [NSUserDefaults standardUserDefaults];
    [substanceImage setObject:imageName forKey:@"substanceImage"];
    
    [self dismissModalViewControllerAnimated:NO];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [imageView release];
    imageView = nil;
    [selectBtn release];
    selectBtn = nil;
    [previousBtn release];
    previousBtn = nil;
    [nextBtn release];
    nextBtn = nil;
    [substanceLbl release];
    substanceLbl = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [imageView release];
    [selectBtn release];
    [previousBtn release];
    [nextBtn release];
    [substanceLbl release];
    [super dealloc];
}
@end
