//
//  TiltingObjects.m
//  iPower
//
//  Created by Daniel Moran on 5/7/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//
//
//#import "TiltingObjects.h"
//#import "SubstanceObject.h"
//#import "Constants.h"
//
//@implementation TiltingObjects
//
//
//+(NSMutableArray *) calculateSubstanceDistanceAndAssignInertiaLevel:(NSMutableArray *)substances {
//    
//    for (SubstanceObject *substance in substances) {
//        
//        if (substance.inertia == 0) {
//            
//            int i = 0;
//            for (i = 0; [substances count]> i; i++) {
//                
//                SubstanceObject *substanceObjectAtIndex = [substances objectAtIndex:i];
//                
//                CGPoint substanceCenterPoint = substance.center;
//                CGPoint substanceCenterAtIndex =  substanceObjectAtIndex.center;
//                CGFloat deltaX = ABS(substanceCenterPoint.x - substanceCenterAtIndex.x);
//                CGFloat deltaY = ABS(substanceCenterPoint.y - substanceCenterAtIndex.y);
//                CGFloat distance = sqrtf(deltaX*deltaX + deltaY*deltaY);
//                
//                if (distance < kObjectSize && !distance == 0) {
//                    substance.inertia = distance;
//                    break;
//                }
//            }
//        }
//    }
//    return substances;
//}
//
//+(NSMutableArray *) moveObjectBasedOnTiltAngleForSubstances:(NSMutableArray *)substances havingAngle:(CGFloat) angle withAcceleration:(UIAcceleration *) acceleration {
//    
//    BOOL moveObject = NO;
//    if (angle > 0.25) {
//        int i = 0;
//        
//        SubstanceObject *substanceObject = [substances objectAtIndex:0];
//        
//        for (i = 0;[substances count]>i; i++){
//            
//            moveObject = NO;
//            
//            substanceObject = [substances objectAtIndex:i];
//    
//            
//            if (substanceObject.inertia >= 9.0f){
//                moveObject = YES;
//            }else if (angle> 0.30 && angle < 0.33 && (substanceObject.inertia > 8.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.33 && angle < 0.36 && (substanceObject.inertia > 7.5f)) {
//                moveObject = YES;
//            }else if (angle> 0.36 && angle < 0.39 && (substanceObject.inertia > 7.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.39 && angle < 0.41 && (substanceObject.inertia > 6.5f)) {
//                moveObject = YES;
//            }else if (angle> 0.41 && angle < 0.44 && (substanceObject.inertia > 6.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.44 && angle < 0.47 && (substanceObject.inertia > 5.5f)) {
//                moveObject = YES;
//            }else if (angle> 0.47 && angle < 0.50 && (substanceObject.inertia > 5.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.53 && angle < 0.55  && (substanceObject.inertia > 4.5f)) {
//                moveObject = YES;
//            }else if (angle> 0.55  && (substanceObject.inertia > 3.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.60  && (substanceObject.inertia > 2.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.65  && (substanceObject.inertia >= 1.0f)) {
//                moveObject = YES;
//            }else if (angle> 0.70  && (substanceObject.inertia >= 0.0f)) {
//                moveObject = YES;
//            }
//        }
//        
//        
//        if (moveObject) {
//            
//            substanceObject.center = CGPointMake(substanceObject.center.x + acceleration.x, substanceObject.center.y - acceleration.y);
//            
//            CGRect movingObject = substanceObject.frame;
//            
//            for (i = 0; [substances count]>i; i++) {
//                
//                SubstanceObject *substanceObjectStanding = [substances objectAtIndex:i];
//                
//                CGRect standingObject = substanceObjectStanding.frame;
//                
//                if (CGRectIntersectsRect(movingObject, standingObject)) {
//                    
//                    substanceObject.inertia = substanceObjectStanding.inertia;
//                }
//            }
//        }
//    }
//    return substances;
//}
//
//@end
