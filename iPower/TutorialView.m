//
//  TutorialView.m
//  iPower
//
//  Created by Daniel Moran on 7/27/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "TutorialView.h"
#import "Constants.h"


@interface TutorialView ()

@property (nonatomic, retain) UIButton *closeViewBtn;
@property (nonatomic, retain) UIImageView *tutorialImageView;

@end


@implementation TutorialView
@synthesize deviceOrientation;
@synthesize closeViewBtn =_closeViewBtn;
@synthesize tutorialImageView = _tutorialImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}




+ (id)tutorial { 
    
    return [[[self alloc] init] autorelease]; 
}

-(UIImageView *) tutorialViewhavingFrame:(CGRect )frame {
    
//    UIImage *image = [UIImage imageNamed:@"labPageBackground.png"];
    self.tutorialImageView = [[UIImageView alloc]initWithFrame:frame];
//    self.tutorialImageView.image = image;
    self.tutorialImageView.backgroundColor = [UIColor yellowColor];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsAppFirstTimeOpened];
    
    UIImage *buttonImage = [UIImage imageNamed:@"closeTutotial.png"];
    self.closeViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeViewBtn.frame = CGRectMake(frame.size.width - 30, 5, 30, 30);
    [self.closeViewBtn addTarget:self 
                     action:@selector(removeTutotialView)
                forControlEvents:UIControlEventTouchDown];
    [self.closeViewBtn setImage:buttonImage forState:UIControlStateNormal];

    UILabel *temp = [[UILabel alloc] initWithFrame:CGRectMake(50, 50, 200, 70)];	
    temp.text = @"Tutorial Content goes here!!!";
    [self.tutorialImageView addSubview:temp];
    
    [self.tutorialImageView addSubview:self.closeViewBtn];
    [self bringSubviewToFront:self.closeViewBtn];
    

    self.tutorialImageView.userInteractionEnabled = YES;


    
    return self.tutorialImageView;
}

-(void) removeTutotialView {
    
    [self.tutorialImageView removeFromSuperview];
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
