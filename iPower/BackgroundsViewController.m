//
//  BackgroundsViewController.m
//  iPower
//
//  Created by Daniel Moran on 2/21/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "BackgroundsViewController.h"
#import "Constants.h"

@interface BackgroundsViewController ()



@end

@implementation BackgroundsViewController
@synthesize backgroundImageCenter;
//@synthesize backgroundImageLeft;
//@synthesize backgroundImageRight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"backgroundsPageBackground.png"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    NSString *selection1 = kBackgroundSelection1;
    NSString *selection2 = kBackgroundSelection2;
    NSString *selection3 = kBackgroundSelection3;
    NSString *selection4 = kBackgroundSelection4;
    
    surfaceImages = [[NSArray arrayWithObjects:selection1, selection2, selection3, selection4,nil]retain];
    
    NSString *surface1Description = kBackgroundDescription1;
    NSString *surface2Description = kBackgroundDescription2;
    NSString *surface3Description = kBackgroundDescription3;
    NSString *surface4Description = kBackgroundDescription4;
    
    surfaceDescription = [[NSArray arrayWithObjects:surface1Description, surface2Description, surface3Description, surface4Description, nil]retain];
    descriptionLbl.text = surface1Description;
    currentIndex = 0;
    
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)] autorelease];
    
    
    UISwipeGestureRecognizer *swipeLeft = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(presentNextBackgroundImage:)]autorelease];                                
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = 
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(presentPreviousBackgroundImage:)] autorelease];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:swipeRight];
}





- (IBAction)presentPreviousBackgroundImage:(id)sender {
    
    currentIndex --;
    if (currentIndex < 0) {
        currentIndex = [surfaceImages count]-1;
    }
    
    [self updateBackgroundImages];    
}

- (IBAction)presentNextBackgroundImage:(id)sender {
    
    currentIndex ++;
    if (currentIndex > [surfaceImages count]-1) {
        currentIndex = 0;
    }
    
    [self updateBackgroundImages];
}

-(void) updateBackgroundImages {
    
    NSString *imageCurrentName = [NSString stringWithString:[surfaceImages objectAtIndex:currentIndex]];
    self.backgroundImageCenter.image = [UIImage imageNamed:imageCurrentName];
    
    int currentIndexMinusOne = currentIndex -1;
    if (currentIndexMinusOne < 0) {
        currentIndexMinusOne = [surfaceImages count]-1;
    }
//    NSString *imagePreviousName = [NSString stringWithString:[surfaceImages objectAtIndex:currentIndexMinusOne]];
//    self.backgroundImageLeft.image = [UIImage imageNamed:imagePreviousName];
    
    int currentIndexPlusOne = currentIndex + 1;
    if (currentIndexPlusOne > [surfaceImages count]-1) {
        currentIndexPlusOne = 0;
    }
//    NSString *imageNextName = [NSString stringWithString:[surfaceImages objectAtIndex:currentIndexPlusOne]];
//    self.backgroundImageRight.image = [UIImage imageNamed:imageNextName];
    
    descriptionLbl.text = [surfaceDescription objectAtIndex:currentIndex];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject]; 
    CGPoint touch_point = [touch locationInView:self.backgroundImageCenter];
    
    if ([self.backgroundImageCenter pointInside:touch_point withEvent:event]) [self selectBackground:nil];
}

- (IBAction)selectBackground:(id)sender {
    
    NSArray *backgroundImageNames = [NSArray arrayWithObjects:kBackground1, kBackground2, kBackground3, kBackground4, nil];
    [self setBackgroundImageHaving:[backgroundImageNames objectAtIndex:currentIndex]];
}

//// the next 4 methods are iPad methods.
//- (IBAction)selectMarbleBackground:(id)sender {
//    
//    [self setBackgroundImageHaving:kBackgroundPad1];
//}
//- (IBAction)selectWoodenBackground:(id)sender {
//    
//    [self setBackgroundImageHaving:kBackgroundPad2];
//}
//- (IBAction)selectLeatherBackground:(id)sender {
//    [self setBackgroundImageHaving:kBackgroundPad3];
//}
//- (IBAction)selectMetalBackground:(id)sender {
//    
//    [self setBackgroundImageHaving:kBackgroundPad4];
//}

-(void) setBackgroundImageHaving:(NSString *) name {
    
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:kBackgroundKeyName];
    [self dismissModalViewControllerAnimated:NO];
}

- (void)viewDidUnload
{

    [selectBtn release];
    selectBtn = nil;
    [nextBtn release];
    nextBtn = nil;
    [previousBtn release];
    previousBtn = nil;
    [descriptionLbl release];
    descriptionLbl = nil;
//    [self setBackgroundImageRight:nil];
//    [self setBackgroundImageLeft:nil];
    [self setBackgroundImageCenter:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [selectBtn release];
    [nextBtn release];
    [previousBtn release];
    [descriptionLbl release];
//    [backgroundImageRight release];
//    [backgroundImageLeft release];
    [backgroundImageCenter release];
    [super dealloc];
}
@end
