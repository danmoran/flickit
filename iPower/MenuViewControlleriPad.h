//
//  MenuViewControlleriPad.h
//  iPower
//
//  Created by Daniel Moran on 5/26/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@class MainSceneViewController;

@interface MenuViewControlleriPad : UIViewController <MFMailComposeViewControllerDelegate>
{
    
    UILabel *mailMessage;
}


@property (retain, nonatomic) IBOutlet UIButton *shareBtn;
@property (retain, nonatomic) IBOutlet UIButton *rateAppBtn;
@property (retain, nonatomic) IBOutlet UIButton *aboutBtn;


@property (retain, nonatomic) IBOutlet UISwitch *backgroundSwitch;
@property (retain, nonatomic) IBOutlet UISwitch *gestureSwitch;
@property (retain, nonatomic) IBOutlet UISwitch *consumeModeSwitch;

+ (id) menu;
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;
-(void) setBackgroundImageHaving:(NSString *) name;

@end
