//
//  MenuViewController.m
//  iPower
//
//  Created by Daniel Moran on 4/4/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "MenuViewController.h"
#import "Appirater.h"
#import "BackgroundsViewController.h"
#import "AboutViewController.h"
#import "SoundsControlViewController.h"
#import "FlickitSharing.h"

@interface MenuViewController ()

@end

@implementation MenuViewController
@synthesize playBtn;
@synthesize backgroundBtn;
@synthesize shareBtn;
@synthesize rateAppBtn;
@synthesize soundBtn;
@synthesize aboutBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        
        
    } else {
        
        NSLog(@"iPad");
        
    }
}



- (IBAction)presentMainSceneViewController:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
    
}
- (IBAction)presentSoundControlView:(id)sender {
    
    SoundsControlViewController *soundsViewController = [[[SoundsControlViewController alloc]init]autorelease];
    
    [self presentModalViewController:soundsViewController animated:NO];
    
//    SoundsControl *soundControlView = [[[SoundsControl alloc]initWithFrame:CGRectZero]autorelease];
//    
////    soundControlView.alpha = 0.0f;
//    [self.view addSubview:soundControlView];
    
}
- (IBAction)presentBackgroundViewController:(id)sender {
    
    BackgroundsViewController *background = [[[BackgroundsViewController alloc]init]autorelease];
    [self presentModalViewController:background animated:NO];
    
}

- (IBAction)presentAboutViewController:(id)sender {
    
    AboutViewController *about = [[[AboutViewController alloc]init]autorelease];
    [self presentModalViewController:about animated:NO];
}

-(void)launchMailAppOnDevice
{
    NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=Hello from California!";
    NSString *body = @"&body=It is raining in sunny California!";
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}

-(void)displayComposerSheet {
    
    
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
    mailComposer.mailComposeDelegate = self;
    
    [mailComposer setSubject:@"Check out this iPhone app"];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"logo114" ofType:@"png"];
    NSData *myData = [NSData dataWithContentsOfFile:path];
    [mailComposer addAttachmentData:myData mimeType:@"image/png" fileName:@"logo114"];
    
    NSString *emailBody = @"I found a really cool app in the iTunes store, check out iPower  <a href=\"http://itunes.apple.com/us/app/free-english-spanish-dictionary/id492611634?ls=1&mt=8\">Click Here</a>";
    [mailComposer setMessageBody:emailBody isHTML:YES];
    
    [self presentModalViewController:mailComposer animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
    mailMessage.hidden = NO;
    switch (result)
    {
        case MFMailComposeResultCancelled:
            mailMessage.text = @"Result: canceled";
            break;
        case MFMailComposeResultSaved:
            mailMessage.text = @"Result: saved";
            break;
        case MFMailComposeResultSent:
            mailMessage.text = @"Result: sent";
            break;
        case MFMailComposeResultFailed:
            mailMessage.text = @"Result: failed";
            break;
        default:
            mailMessage.text = @"Result: not sent";
            break;
    }
    [self dismissModalViewControllerAnimated:YES];
    
}

- (IBAction)rateApp:(id)sender {
    
    NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=492611634";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSString *reviewURL = templateReviewURL;
	[userDefaults setBool:YES forKey:kAppiraterRatedCurrentVersion];
	[userDefaults synchronize];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
    
}

- (void)shareApp
{
    if (self.view)
    {
        [FlickitSharing appShareGeneralInfoFromView:self.view];
    }
}

- (IBAction)shareApp:(id)sender {
    
    [self shareApp];

//    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
//    if (mailClass != nil)
//    {
//        if ([mailClass canSendMail])
//        {
//            [self displayComposerSheet];
//        }
//        else
//        {
//            [self launchMailAppOnDevice];
//        }
//    }
//    else
//    {
//        [self launchMailAppOnDevice];
//    }
}

- (void)viewDidUnload
{
    [self setPlayBtn:nil];
    [self setBackgroundBtn:nil];
    [self setShareBtn:nil];
    [self setRateAppBtn:nil];
    [self setSoundBtn:nil];
    [self setAboutBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [playBtn release];
    [backgroundBtn release];
    [shareBtn release];
    [rateAppBtn release];
    [soundBtn release];
    [aboutBtn release];
    [super dealloc];
}
@end
