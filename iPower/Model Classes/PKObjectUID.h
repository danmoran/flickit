//
//  PKObjectUID.h
//  PKTest
//
//  Created by Petr Homola on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKObjectUID : NSObject <NSCopying> {
	NSString* UID;
}

@property (nonatomic, readonly) NSString* UID;

- (id)initWithUID:(NSString*)_UID;
+ (PKObjectUID*)objectUID;

@end
