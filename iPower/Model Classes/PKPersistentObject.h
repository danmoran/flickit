//
//  PKPersistentObject.h
//  PKTest
//
//  Created by Petr Homola on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PKPersistentObject

- (id)valueForKey:(id)key;

@end
