//
//  PKPredicate.m
//  PKTest
//
//  Created by Petr Homola on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PKPredicate.h"
#import "PKAttributeValuePredicate.h"
#import "PKCompositePredicate.h"

@implementation PKPredicate

+ (PKPredicate*)predicateWithAttributeName:(NSString*)attName value:(id)_value {
	return [[[PKAttributeValuePredicate alloc] initWithAttributeName: attName value: _value]autorelease]; // ARC
}

+ (PKPredicate*)predicateWithAttributeNames:(NSArray*)attNames values:(id)_values {
	PKCompositePredicate* predicate = [[[PKCompositePredicate alloc] initWithComposition: kPKPredicateCompositionAnd]autorelease]; // ARC
	for (int i = 0; i < [attNames count]; i++) {
		[predicate addPredicate: [PKPredicate predicateWithAttributeName: [attNames objectAtIndex: i]
																   value: [_values objectAtIndex: i]]];
	}
	return predicate;
}

- (BOOL)matchesObject:(id<PKPersistentObject>)object {
	return NO; // this is an abstract class so we'll return NO for any object
}

@end
