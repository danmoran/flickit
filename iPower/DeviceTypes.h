//
//  DeviceTypes.h
//  iPower
//
//  Created by Daniel Moran on 8/12/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceTypes : NSObject


//+ (id)deviceType;

//- (NSString *)DeviceName;
//- (void)doEachDevice;
+ (CGFloat) determineDeviceTypeMultiplier;
+ (NSInteger) circleHolderSizeHavingMultiplier:(CGFloat) multiplier;

@end
