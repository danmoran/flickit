//
//  LabView.h
//  iPower
//
//  Created by Daniel Moran on 4/2/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainSceneViewController;

@interface LabView : UIView


-(void) loadInterface;
-(void) removeViewFromMainScene;
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;
-(void) presentSavedMixturesViewController;
-(void) presentMenuViewController;
-(void) presentSavePopupView;
-(void) updateSliders:(NSString *) container;
-(void) passValuesToSlidersFromDictionary:(NSDictionary *)containerValues;
-(void) slider1Changed;
-(void) slider2Changed;
-(void) slider3Changed;
-(void) slider4Changed;
-(void) updateContainerLabels;

@end
