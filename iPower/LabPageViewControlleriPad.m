//
//  LabPageViewControlleriPad.m
//  iPower
//
//  Created by Daniel Moran on 7/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "LabPageViewControlleriPad.h"
#import "Constants.h"
#import "MainSceneViewController.h"
#import "SavedMixturesViewController.h"
#import "SaveMixtureViewController.h"

@interface LabPageViewControlleriPad ()

//@property (nonatomic, retain) MainSceneViewController *mainScene;
@property (nonatomic, retain) SavedMixturesViewController *savedMixturesListViewController;
@property (nonatomic, retain) SaveMixtureViewController *saveMixture;
@property (nonatomic, retain) UIPopoverController *popoverMixtureList;
@property (nonatomic, retain) UIPopoverController *popoverSave;
@property (nonatomic, assign) CGFloat container1;
@property (nonatomic, assign) CGFloat container2;
@property (nonatomic, assign) CGFloat container3;
@property (nonatomic, assign) CGFloat container4;
@property (nonatomic, assign) BOOL is100Percent;

@end

@implementation LabPageViewControlleriPad

@synthesize mainScene = _mainScene;
@synthesize savedMixturesListViewController = _savedMixturesListViewController;
@synthesize saveMixture = _saveMixture;
@synthesize popoverMixtureList = _popoverMixtureList;
@synthesize popoverSave = _popoverSave;
@synthesize slider1;
@synthesize slider2;
@synthesize slider3;
@synthesize slider4;
@synthesize description;
@synthesize slider1Label;
@synthesize slider2Label;
@synthesize slider3Label;
@synthesize slider4Label;

@synthesize container1, container2, container3, container4;
@synthesize is100Percent;

@synthesize selectionHandler;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.saveMixture = [SaveMixtureViewController save];
        [self.saveMixture setLabPageViewControlleriPad:self];
        // Custom initialization
    }
    return self;
}

+ (id) labPage { 
    
    return [[[self alloc] init] autorelease]; 
}
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController {
    
    self.mainScene = _mainSceneViewController;
}

-(void) saveMixtureHavingName:(NSString *)name {
    NSLog(@"test");
    [self.mainScene saveMixtureInDBWithName:name];
}


- (void)viewDidLoad
{
    

    [super viewDidLoad];
    UIImage *minImage = [UIImage imageNamed:@"sliderBarWhite.png"];
    UIImage *maxImage = [UIImage imageNamed:@"sliderBarEmpty.png"];
    UIImage *tumbImage = [UIImage imageNamed:@"sliderWhiteBtn.png"];
    /// trans must be -0.5 in order for min value to be at bottom
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
//    self.slider1 = [[[UISlider alloc]initWithFrame:CGRectMake(-55, 140, 200, 80)]autorelease];
    [self.slider1 setMinimumTrackImage:minImage forState:UIControlStateNormal];
	[self.slider1 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
	[self.slider1 setThumbImage:tumbImage forState:UIControlStateNormal];
    [self.slider1 addTarget:self action:@selector(slider1Changed) 
           forControlEvents:UIControlEventValueChanged];
    self.slider1.transform = trans;    self.slider1.continuous = NO;
    
    minImage = [UIImage imageNamed:@"sliderBarGreen.png"];
    tumbImage = [UIImage imageNamed:@"sliderGreenBtn.png"]; 
    [self.slider2 setMinimumTrackImage:minImage forState:UIControlStateNormal];
	[self.slider2 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
	[self.slider2 setThumbImage:tumbImage forState:UIControlStateNormal];
    [self.slider2 addTarget:self action:@selector(slider2Changed) 
           forControlEvents:UIControlEventValueChanged];
    self.slider2.transform = trans;
    self.slider2.continuous = NO;
    
    minImage = [UIImage imageNamed:@"sliderBarBlue.png"];
    tumbImage = [UIImage imageNamed:@"sliderBlueBtn.png"];
    [self.slider3 setMinimumTrackImage:minImage forState:UIControlStateNormal];
	[self.slider3 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
	[self.slider3 setThumbImage:tumbImage forState:UIControlStateNormal];
    [self.slider3 addTarget:self action:@selector(slider3Changed) 
           forControlEvents:UIControlEventValueChanged];
    self.slider3.transform = trans;
    self.slider3.continuous = NO;
    
    minImage = [UIImage imageNamed:@"sliderBarPink.png"];
    tumbImage = [UIImage imageNamed:@"sliderPinkBtn.png"];
    [self.slider4 setMinimumTrackImage:minImage forState:UIControlStateNormal];
	[self.slider4 setMaximumTrackImage:maxImage forState:UIControlStateNormal];
	[self.slider4 setThumbImage:tumbImage forState:UIControlStateNormal];
    [self.slider4 addTarget:self action:@selector(slider4Changed) 
           forControlEvents:UIControlEventValueChanged];
    self.slider4.transform = trans;
    self.slider4.continuous = NO;

}

-(void) slider1Changed {
    
    self.description.text = kContainerDescription1;
    [self updateSliders:kContainer1]; 
    
}
-(void) slider2Changed { 
    
    self.description.text = kContainerDescription2;    
    [self updateSliders:kContainer2]; 
}
-(void) slider3Changed { 
    
    self.description.text = kContainerDescription3; 
    [self updateSliders:kContainer3]; 
}
-(void) slider4Changed { 
    
    self.description.text = kContainerDescription4; 
    [self updateSliders:kContainer4]; 
}

-(void) updateSliders:(NSString *) container {
    
    CGFloat differenceInValue = 0;
    CGFloat sumContainer;
    CGFloat tempContainerValue1 = 0.0f;
    CGFloat tempContainerValue2 = 0.0f;
    CGFloat tempContainerValue3 = 0.0f;
    CGFloat tempContainerValue4 = 0.0f;
    CGFloat sumContainers = slider1.value + slider2.value + slider3.value + slider4.value;
    
    if (sumContainers > 1) {
        
        if (container == kContainer1) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider1.value - container1;
            }
            sumContainer = container2 + container3 + container4;
            
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }      
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }      
            if (differenceInValue>0) {
                container2 = container2 - tempContainerValue2;
                container3 = container3 - tempContainerValue3;
                container4 = container4 - tempContainerValue4;
            }            
            container1  = slider1.value;
            slider2.value = container2;
            slider3.value = container3;
            slider4.value = container4;
            
            [self updateContainerLabels];
            
            
        } else if (container == kContainer2) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider2.value - container2;
            }
            sumContainer = container1 + container3 + container4;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container3 = container3 - tempContainerValue3;
                container4 = container4 - tempContainerValue4;
            }
            container2  = slider2.value;
            slider1.value = container1;
            slider3.value = container3;
            slider4.value = container4;
            
            [self updateContainerLabels];
            
        } else if (container == kContainer3) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider3.value - container3;
            }
            sumContainer = container1 + container2 + container4;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container4 > 0) {
                tempContainerValue4 = (container4/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container2 = container2 - tempContainerValue2;
                container4 = container4 - tempContainerValue4;
            }
            container3  = slider3.value;
            slider1.value = container1;
            slider2.value = container2;
            slider4.value = container4;
            
            [self updateContainerLabels];
            
        }else if (container == kContainer4) {
            
            if (!is100Percent) {
                differenceInValue =  sumContainers - 1;
                is100Percent = YES;
            }
            else {
                differenceInValue = slider4.value - container4;
            }
            sumContainer = container1 + container2 + container3;
            
            if (container1 > 0) {
                tempContainerValue1 = (container1/sumContainer)*differenceInValue;
            }
            if (container2 > 0) {
                tempContainerValue2 = (container2/sumContainer)*differenceInValue;
            }
            if (container3 > 0) {
                tempContainerValue3 = (container3/sumContainer)*differenceInValue;
            }  
            
            if (differenceInValue>0) {
                
                container1 = container1 - tempContainerValue1;
                container2 = container2 - tempContainerValue2;
                container3 = container3 - tempContainerValue3;
            }
            container4  = slider4.value;
            slider1.value = container1;
            slider2.value = container2;
            slider3.value = container3;
            
            [self updateContainerLabels];
        }
        
        
    }else {
        is100Percent = NO;
        container1 = slider1.value;
        container2 = slider2.value;
        container3 = slider3.value;
        container4 = slider4.value;
        
        [self updateContainerLabels];
    }
    
    NSMutableDictionary *tempContainerValues = [NSMutableDictionary dictionary];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider1.value] forKey:kContainer1];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider2.value] forKey:kContainer2];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider3.value] forKey:kContainer3];
    [tempContainerValues setObject:
     [NSNumber numberWithDouble:slider4.value] forKey:kContainer4];
    
    [self.mainScene updateSubstanceTypesOnScreenWithValuesIn:tempContainerValues];
}
-(void) passValuesToSlidersFromDictionary:(NSDictionary *)containerValues {
    
    slider1.value = [[containerValues objectForKey:kContainer1] floatValue];
    slider2.value = [[containerValues objectForKey:kContainer2] floatValue];
    slider3.value = [[containerValues objectForKey:kContainer3] floatValue];
    slider4.value = [[containerValues objectForKey:kContainer4] floatValue];    
    
    container1 = slider1.value;
    container2 = slider2.value;
    container3 = slider3.value;
    container4 = slider4.value;
    
    [self updateContainerLabels];
    
}
-(void) updateContainerLabels {
    
    self.slider1Label.text = [NSString stringWithFormat:@"%.0f %%", slider1.value * 100];
    self.slider2Label.text = [NSString stringWithFormat:@"%.0f %%", slider2.value * 100];
    self.slider3Label.text = [NSString stringWithFormat:@"%.0f %%", slider3.value * 100];
    self.slider4Label.text = [NSString stringWithFormat:@"%.0f %%", slider4.value * 100];
}
- (IBAction)dismissLabPageViewController:(id)sender {
    
    selectionHandler ();
    
}

- (IBAction)presentSavedMixturesList:(id)sender {

    self.savedMixturesListViewController = [[[SavedMixturesViewController alloc]initWithNibName:@"SavedMixturesViewControlleriPad" bundle:nil]autorelease];
    
    self.popoverMixtureList = [[[UIPopoverController alloc]initWithContentViewController:self.savedMixturesListViewController]autorelease];
    self.popoverMixtureList.popoverContentSize=CGSizeMake(250,460);
    self.popoverMixtureList.delegate = self;

    CGRect selectedRect = CGRectMake(200,300,1,1);
    [self.popoverMixtureList presentPopoverFromRect:selectedRect inView:self.view permittedArrowDirections:0 animated:YES]; 
    
    self.savedMixturesListViewController.selectionHandler = ^{
        
        [self.popoverMixtureList dismissPopoverAnimated:YES];          
    };
}

- (IBAction)presentSaveView:(id)sender {
    
//    SaveMixtureViewController *saveViewController = [[[SaveMixtureViewController alloc]initWithNibName:@"SaveMixtureViewController" bundle:nil]autorelease];
    
    self.popoverSave = [[[UIPopoverController alloc]initWithContentViewController:self.saveMixture]autorelease];
    self.popoverSave.popoverContentSize=CGSizeMake(250,175);
    self.popoverSave.delegate = self;
    
    CGRect selectedRect = CGRectMake(200,300,1,1);
    [self.popoverSave presentPopoverFromRect:selectedRect inView:self.view permittedArrowDirections:0 animated:YES]; 
    
    self.saveMixture.selectionHandler = ^{
        
        [self.popoverSave dismissPopoverAnimated:YES];          
    };
}


- (void)dealloc {
    
   
    [description release];
    [slider3 release];
    [slider4 release];
    [slider1Label release];
    [slider2Label release];
    [slider3Label release];
    [selectionHandler release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [self setSlider1:nil];
    [self setSlider2:nil];
    [self setDescription:nil];
    [self setSlider3:nil];
    [self setSlider4:nil];
    [self setSlider1Label:nil];
    [self setSlider2Label:nil];
    [self setSlider3Label:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


@end
