//
//  NSObjectPKAdditions.h
//  PKTest
//
//  Created by Petr Homola on 6/26/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSObject)

- (NSString*)className;

@end
