//
//  MixturesObject.h
//  iPower
//
//  Created by Daniel Moran on 4/10/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKPersistentObject.h"

@interface MixturesObject : NSObject <PKPersistentObject>

@property (nonatomic, retain) NSMutableArray *centerPoints;
@property (nonatomic, retain) NSMutableArray *types;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSMutableArray *imageNames;
//@property (nonatomic, retain) NSMutableArray *inertiaValues;
@property (nonatomic, retain) NSDate *dateSaved;

@end
