//
//  AppDelegate.m
//  iPower
//
//  Created by Daniel Moran on 2/11/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "AppDelegate.h"
#import "MainSceneViewController.h"
#import "Appirater.h"
#import "Constants.h"

#import "FlickitSHKConfigurator.h"
#import "SHKConfiguration.h"
#import "SHKFacebook.h"

@implementation AppDelegate

@synthesize window = _window;
//@synthesize viewController = _viewController;
//@synthesize mainPage = _mainPage;
@synthesize mainScene = _mainScene;

- (void)dealloc
{
    [_window release];
//    [_mainPage release];
    [_mainScene release];
//    [_viewController release];
    [super dealloc];
}

// sharekit integration for sso
- (BOOL)handleOpenURL:(NSURL*)url
{
    NSString* scheme = [url scheme];
    NSString* prefix = [NSString stringWithFormat:@"fb%@", SHKCONFIG(facebookAppId)];
    if ([scheme hasPrefix:prefix])
        return [SHKFacebook handleOpenURL:url];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation 
{
    return [self handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url 
{
    return [self handleOpenURL:url];  
}

- (void) initSharekit
{
    DefaultSHKConfigurator * configurator = [[FlickitSHKConfigurator alloc] init];
    [SHKConfiguration sharedInstanceWithConfigurator:configurator];
    [configurator release];
    
    [SHK flushOfflineQueue];
}
// end sharekit integration

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *defaultSettings1 = [NSUserDefaults standardUserDefaults];
    BOOL wasPreviouslyOpened = [defaultSettings1 boolForKey:kWasPreviouslyOpened];
    
    if (!wasPreviouslyOpened) {
        [defaultSettings1 setBool:YES forKey:kWasPreviouslyOpened];
//        [defaultSettings1 setBool:NO forKey:kPlayBackgroundMusic];
        [defaultSettings1 setBool:YES forKey:kPlayDraggingSound];
        [defaultSettings1 setBool:YES forKey:kPlayConsumeSound];
    }

    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

        self.mainScene = [[[MainSceneViewController alloc]initWithNibName:@"MainSceneViewController" bundle:nil]autorelease];
        
    } else {
        
        self.mainScene = [[[MainSceneViewController alloc]initWithNibName:@"MainSceneViewControllerPad" bundle:nil]autorelease];
        
    }

    
    self.window.rootViewController = self.mainScene;
    [self.window makeKeyAndVisible];
    
    [Appirater appLaunched:YES];
    
    [self initSharekit];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
