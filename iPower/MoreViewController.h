//
//  MoreViewController.h
//  iPower
//
//  Created by Daniel Moran on 2/22/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MoreViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    UILabel *mailMessage;
    
    IBOutlet UIButton *menuBtn;
    IBOutlet UIButton *tellOthersBtn;
    IBOutlet UIButton *upgradeToFullVersionBtn;
    IBOutlet UIButton *rateAppBtn;
    IBOutlet UIButton *aboutBtn;
}


-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;


@end
