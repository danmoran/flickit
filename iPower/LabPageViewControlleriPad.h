//
//  LabPageViewControlleriPad.h
//  iPower
//
//  Created by Daniel Moran on 7/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MainSceneViewController;

@interface LabPageViewControlleriPad : UIViewController <UIPopoverControllerDelegate>

@property (nonatomic, retain) MainSceneViewController *mainScene;
@property (retain, nonatomic) IBOutlet UISlider *slider1;
@property (retain, nonatomic) IBOutlet UISlider *slider2;
@property (retain, nonatomic) IBOutlet UISlider *slider3;
@property (retain, nonatomic) IBOutlet UISlider *slider4;
@property (retain, nonatomic) IBOutlet UILabel *slider1Label;
@property (retain, nonatomic) IBOutlet UILabel *slider2Label;
@property (retain, nonatomic) IBOutlet UILabel *slider3Label;
@property (retain, nonatomic) IBOutlet UILabel *slider4Label;
@property (nonatomic, copy) void (^selectionHandler)();

@property (retain, nonatomic) IBOutlet UILabel *description;

+ (id) labPage;
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;

-(void) saveMixtureHavingName:(NSString *)name;

@end
