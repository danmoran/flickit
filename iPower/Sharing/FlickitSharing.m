//
//  FlickitSharing.m
//  iPower
//
//  Created by edward liveikis on 5/21/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "FlickitSharing.h"

#import "SHK.h"
#import "SHKFacebook.h"
#import <UIKit/UIKit.h>

@implementation FlickitSharing

+ (void)appShareGeneralInfoFromView:(UIView *)targetView
{
    if (targetView)
    {
        SHKItem *item = [SHKItem text:@"I am using iPower"];
        
        // Get the ShareKit action sheet
        SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
        
        // Display the action sheet
        [actionSheet showInView:targetView];
        //[actionSheet showFromRect:targetView.frame inView:targetView animated:YES];
    }
}

+ (void)shareImage:(UIImage *)imageToShare
{
    if (imageToShare)
    {
        // Create the item to share (in this example, a url)
        SHKItem *item = [SHKItem image:imageToShare title:@"My Flickit"];
        item.shareType = SHKShareTypeImage;
        
        SHKSharer* sharer = [[SHKFacebook alloc] init];

        if (![sharer isAuthorized])
        {
            [sharer authorize];
        }

        [sharer loadItem:item];
        [sharer share];
        
        [sharer release];
    }
}

+ (void)shareImage:(UIImage *)imageToShare inView:(UIView *)targetView
{
    // Create the item to share (in this example, a url)
    SHKItem *item = [SHKItem image:imageToShare title:@"My Flick-it Mix"];
    item.shareType = SHKShareTypeImage;
    
    // Get the ShareKit action sheet
    SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    
    // ShareKit detects top view controller (the one intended to present ShareKit UI) automatically,
    // but sometimes it may not find one. To be safe, set it explicitly
    //[SHK setRootViewController:self];
    
    // Display gitthe action sheet
    [actionSheet showFromRect:targetView.frame inView:targetView animated:YES];// showFromToolbar:toolbar];

}

@end
