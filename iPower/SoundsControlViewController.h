//
//  SoundsControlViewController.h
//  iPower
//
//  Created by Daniel Moran on 5/15/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoundsControlViewController : UIViewController


@property (retain, nonatomic) IBOutlet UISwitch *backgroundSwitch;
@property (retain, nonatomic) IBOutlet UISwitch *swipesSwitch;
@property (retain, nonatomic) IBOutlet UISwitch *consumeSwitch;

@end
