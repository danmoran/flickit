//
//  PKObjectUID.m
//  PKTest
//
//  Created by Petr Homola on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PKObjectUID.h"

@implementation PKObjectUID

@synthesize UID;

- (id)init {
	if ((self = [self initWithUID: [NSString stringWithFormat: @"%f:%ld", [[NSDate date] timeIntervalSinceReferenceDate], random()]])) {
		// ...
	}
	return self;
}

- (id)initWithUID:(NSString*)_UID {
	if ((self = [super init])) {
		UID = [_UID copy];
	}
	return self;
}

- (id)copyWithZone:(NSZone*)zone {
	return [[PKObjectUID allocWithZone: zone] initWithUID: UID];
}

+ (PKObjectUID*)objectUID {
	return [[[self alloc] init]autorelease]; // ARC
}

- (BOOL)isEqual:(id)object {
	return [object isKindOfClass: [PKObjectUID class]] && [UID isEqualToString: [object UID]];
}

- (NSString*)description {
	return UID;
}


@end
