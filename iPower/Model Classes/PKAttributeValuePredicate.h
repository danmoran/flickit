//
//  PKAttributeValuePredicate.h
//  PKTest
//
//  Created by Petr Homola on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKPredicate.h"

@interface PKAttributeValuePredicate : PKPredicate {
	NSString* attributeName;
	id value;
}

- (id)initWithAttributeName:(NSString*)attName value:(id)_value;

@end
