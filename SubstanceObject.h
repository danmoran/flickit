//
//  SubstanceObject.h
//  iPower
//
//  Created by daniel moran on 3/6/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainSceneViewController;

@interface SubstanceObject : UIImageView

@property (nonatomic, assign) int startingAtCount;
@property (nonatomic, assign) CGPoint centerPoint;
//@property (nonatomic, assign) CGFloat inertia;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSString *substanceType;

@property (nonatomic, retain) MainSceneViewController *mainScene;


-(CGPoint) viewsCenterPointWith:(CGFloat)xValue andYValue:(CGFloat)yValue;

//-(NSMutableArray *) loadSubstanceObjectsWithMaxDist:(int) dist1 minDist:(int)dist2 startingCountAt:(int) startingCount randomlocation:(BOOL) useRandomPlacement forView:(UIView *)view;


//-(NSMutableArray *) test;

-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;
@end
