//
//  SaveMixtureViewController.h
//  iPower
//
//  Created by Daniel Moran on 7/10/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>
//@class MainSceneViewController;
@class LabPageViewControlleriPad;

@interface SaveMixtureViewController : UIViewController

@property (nonatomic, copy) void (^selectionHandler)();
@property (retain, nonatomic) IBOutlet UITextField *saveTextField;


+ (id) save;
//-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;
-(void) setLabPageViewControlleriPad:(LabPageViewControlleriPad *) _labPageViewControlleriPad;

@end
