//
//  SubstancesViewController.h
//  iPower
//
//  Created by Daniel Moran on 2/22/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubstancesViewController : UIViewController {
    
    NSArray *substanceImages;
    NSArray *substanceDescription;
    NSInteger currentIndex;
    IBOutlet UIButton *eraseBtn;

    
    IBOutlet UIButton *selectBtn;
    IBOutlet UIButton *previousBtn;
    IBOutlet UIButton *nextBtn;
    IBOutlet UIImageView *imageView;
    
    IBOutlet UILabel *substanceLbl;
    
    
}

@end
