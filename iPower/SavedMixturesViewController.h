//
//  SavedMixturesViewController.h
//  iPower
//
//  Created by Daniel Moran on 4/5/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainSceneViewController;

@interface SavedMixturesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *dataTableView;
@property (retain, nonatomic) IBOutlet UIButton *editBtn;
@property (retain, nonatomic) IBOutlet UIButton *backBtn;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (nonatomic, copy) void (^selectionHandler)();


-(void) populateTableView;
-(void) setMainSceneViewController:(MainSceneViewController *) _mainSceneViewController;

@end
