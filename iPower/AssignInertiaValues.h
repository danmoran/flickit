//
//  AssignInertiaValues.h
//  iPower
//
//  Created by Daniel Moran on 5/4/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssignInertiaValues : NSObject

+(NSArray *) calculateSubstanceDistanceAndAssignInertiaLevel:(NSArray *)substances;

@end
