//
//  DeviceTypes.m
//  iPower
//
//  Created by Daniel Moran on 8/12/12.
//  Copyright (c) 2012 TapCrew. All rights reserved.
//

#import "DeviceTypes.h"

@implementation DeviceTypes

//+ (id)deviceType {
//    
//    return [[[self alloc] init] autorelease];
//}

//- (NSString *)DeviceName
//{
//    char    cDeviceName[128] = {0,};                // enough to be copied 128
//    size_t  SizeOfDeviceName = sizeof(cDeviceName); // must be initialized by sizeof().
//    
//    sysctlbyname("hw.machine", cDeviceName, &SizeOfDeviceName, NULL, 0);
//    return [[[NSString alloc] initWithFormat:@"%s",cDeviceName] autorelease];
//}

+ (CGFloat)determineDeviceTypeMultiplier
{
    
    char    cDeviceName[128] = {0,};                // enough to be copied 128
    size_t  SizeOfDeviceName = sizeof(cDeviceName); // must be initialized by sizeof().
    
//    TODO: find out what the warning below means.
    sysctlbyname("hw.machine", cDeviceName, &SizeOfDeviceName, NULL, 0);
    NSString *aDeviceType =  [[[NSString alloc] initWithFormat:@"%s",cDeviceName] autorelease];
//    NSString *aDeviceType = [[NSString alloc] initWithString:[self DeviceName]];
    CGFloat multiplier = 0;
    
    if ([aDeviceType isEqualToString:@"i386"])
    {
        NSLog(@"i386 maybe simulator");
    }
    else if ([aDeviceType isEqualToString:@"iPhone1,1"]) // iPhone
    {
        NSLog(@"iPhone");
        multiplier = 0.74;
    }
    else if ([aDeviceType isEqualToString:@"iPhone1,2"]) // iPhone 3G
    {
        NSLog(@"iPhone 3G");
        multiplier = 0.74;
    }
    else if ([aDeviceType isEqualToString:@"iPhone2,1"]) // iPhone 3GS
    {
        NSLog(@"iPhone 3Gs");
        multiplier = 1;
    }
    else if ([aDeviceType isEqualToString:@"iPhone3,1"]) // iPhone 4
    {
        NSLog(@"iPhone 4");
        multiplier = 1;
    }
    else if ([aDeviceType isEqualToString:@"iPhone3,2"]) // iPhone 4S
    {
        NSLog(@"iPhone 4S");
        multiplier = 1;
    }
    else if ([aDeviceType isEqualToString:@"iPhone4,1"]) // iPhone 5
    {
        NSLog(@"iPhone 5");
        multiplier = 1;
    }
    else if ([aDeviceType isEqualToString:@"iPod1,1"]) // 1st Gen iPod
    {
        NSLog(@"iPod 1st Gen");
        multiplier = 0.74;
    }
    else if ([aDeviceType isEqualToString:@"iPod2,1"]) // 2nd Gen iPod
    {
        NSLog(@"iPod 2nd Gen");
        multiplier = 0.74;
    }
    else if ([aDeviceType isEqualToString:@"iPod3,1"]) // 3rd Gen iPod touch 4
    {
        NSLog(@"iPod touch 4 (3rd Gen) like iPhone 4");
        multiplier = 1.0;
    }
    else if ([aDeviceType isEqualToString:@"iPad1,1"]) // 1st Gen iPad
    {
        NSLog(@"iPad 1st Gen");
        multiplier = 1;
    }
    else if ([aDeviceType isEqualToString:@"iPad2,1"]) // 2nd Gen iPad in 2011
    {
        NSLog(@"iPad 2nd Gen in 2011");
        multiplier = 2.0;
    }
    else if ([aDeviceType isEqualToString:@"iPad3,1"]) // 3nd Gen iPad in 2012
    {
        NSLog(@"iPad 3rd Gen in 2012");
        multiplier = 2.0;
    }
    else
    {
       multiplier = 2.0;
    }
    
    
    return multiplier;
}

+ (NSInteger) circleHolderSizeHavingMultiplier:(CGFloat) multiplier {
    
    NSInteger circleSize = 0;
    
    if (multiplier<1) {
        return circleSize = 35;
    } else if (multiplier ==1) {
        return circleSize = 41.3;
    } else if (multiplier > 1) {
        return circleSize = 59;
    }    
    
    return circleSize;
}

//+(NSInteger) determineDeviceType {
//    
//    NSString *aDeviceType = [UIDevice currentDevice].model;
//
//    NSInteger multiplier = 0;
//    
//    
//    if ([aDeviceType isEqualToString:@"i386"])
//    {
//        NSLog(@"i386 maybe simulator");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPhone1,1"]) // iPhone
//    {
//        NSLog(@"iPhone");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPhone1,2"]) // iPhone 3G
//    {
//        NSLog(@"iPhone 3G");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPhone2,1"]) // iPhone 3GS
//    {
//        NSLog(@"iPhone 3Gs");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPhone3,1"]) // iPhone 4
//    {
//        NSLog(@"iPhone 4");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPod1,1"]) // 1st Gen iPod
//    {
//        NSLog(@"iPod 1st Gen");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPod2,1"]) // 2nd Gen iPod
//    {
//        NSLog(@"iPod 2nd Gen");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPod3,1"]) // 3rd Gen iPod touch 4
//    {
//        NSLog(@"iPod touch 4 (3rd Gen) like iPhone 4");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPad1,1"]) // 1st Gen iPad
//    {
//        NSLog(@"iPad 1st Gen");
//        multiplier = 1;
//    }
//    else if ([aDeviceType isEqualToString:@"iPad2,1"]) // 2nd Gen iPad in 2011
//    {
//        NSLog(@"iPad 2nd Gen in 2011");
//        multiplier = 2;
//    }
//    else if ([aDeviceType isEqualToString:@"iPad3,1"])
//    {
//        NSLog(@"iPad 3rd Gen in 2012");
//        multiplier = 2;
//    }
//    return multiplier;
//}

@end
